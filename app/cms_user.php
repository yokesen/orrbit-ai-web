<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cms_user extends Model
{
    protected $table='cms_users';
    protected $guarded = [];
}
