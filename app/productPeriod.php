<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productPeriod extends Model
{
    protected $table = 'product_periods';
    public $timestamps = false;
    protected $guarded = [];

}
