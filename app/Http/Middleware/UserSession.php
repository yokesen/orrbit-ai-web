<?php

namespace App\Http\Middleware;

use Closure;
use crudbooster;

class UserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(crudbooster::myPrivilegeId()=='3'){
            return $next($request);
        }else{
            return redirect()->route('getLogin');
        }
    }
}
