<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use CRUDBooster;
use DB;
use Session;
use Validator;
use Illuminate\Support\Facades\Hash;
use Storage;

class UserController extends Controller
{
    public function postRegister(Request $request){

      // $validator = Validator::make($request->all(), [
      //     'fullname' => 'required|string|min:3',
      //     'email' => 'required|email',
      //     'password' => 'required|confirmed|min:6',
      // ]);
      //
      // if ($validator->fails()) {
      //     $message = $validator->errors()->all();
      //
      //     return redirect()->back()->with(['errors' => implode(', ', $message), 'message_type' => 'danger']);
      // }

      $validator = $request->validate([
        'firstName' => 'required|string|min:3',
        'lastName' => 'required|string|min:3',
        'email' => 'required|email',
        'password' => 'required|confirmed|min:6',
      ]);

      $insert = DB::table('cms_users')->insertGetId([
        'name' => $request->firstName,
        'lastName' => $request->lastName,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'id_cms_privileges' => '3',
        'created_at' => date('Y-m-d H:i:s')
      ]);

      if($insert){

        $users = DB::table(config('crudbooster.USER_TABLE'))->where("id", $insert)->first();
        $priv = DB::table("cms_privileges")->where("id", $users->id_cms_privileges)->first();

        $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $users->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();

        $photo = ($users->photo) ? asset($users->photo) : asset('vendor/crudbooster/avatar.jpg');
        Session::put('admin_id', $users->id);
        Session::put('admin_is_superadmin', $priv->is_superadmin);
        Session::put('admin_name', $users->name);
        Session::put('admin_photo', $photo);
        Session::put('admin_privileges_roles', $roles);
        Session::put("admin_privileges", $users->id_cms_privileges);
        Session::put('admin_privileges_name', $priv->name);
        Session::put('admin_lock', 0);
        Session::put('theme_color', $priv->theme_color);
        Session::put("appname", CRUDBooster::getSetting('appname'));

        CRUDBooster::insertLog(trans("crudbooster.log_login", ['email' => $users->email, 'ip' => $request->ip()]));

        $cb_hook_session = new \App\Http\Controllers\CBHook;
        $cb_hook_session->afterLogin();

        return redirect()->route('userHome');
      }else{
        return redirect()->back();
      }
    }

    // controller from dimas
    public function settingProfile(){
      $user = DB::table('cms_users')
      ->where('id', CRUDBooster::myId())
      ->select('name','email', 'phoneNumber', 'lastName')
      ->get();


      return view('webpages.sub_pages_setting.profil', compact('user'));
  }

  public function submitBlog(Request $request){
    $validator = $request->validate([
      'konten' => 'required'
      ]);
  }
  public function test(){
    return view('webpages.sub_pages_setting.test');
  }

  public function submitProfile(Request $request){
    // dd($request->konten);
    $validator = $request->validate([
      'firstName' => 'required|string|min:2',
      'lastName' => 'required|string|min:2',
      'phoneNumber' => 'numeric',
      ]);
      
      if($request->phoneNumber){
        DB::table('cms_users')
      ->where('id', CRUDBooster::myId())
      ->update([
          'name' => $request->firstName,
          'lastName' => $request->lastName,
          'phoneNumber' => $request->phoneNumber,
      
      ]);
      }else{
        DB::table('cms_users')
        ->where('id', CRUDBooster::myId())
        ->update([
            'name' => $request->firstName,
            'lastName' => $request->lastName,        
        ]);
      }
      return redirect()->back();
  }
  public function privacyPolicy(){
      return view('webpages.sub_pages_setting.privacy-policy');
  }
  public function changePassword(){
      $userData = DB::table('cms_users')
      ->where('id',  CRUDBooster::myId())
      ->select('providerOrigin')
      ->first();

      return view('webpages.sub_pages_setting.change-password', compact('userData'));
  }
  public function submitChangePassword(Request $request){
      $oldPassword = DB::table('cms_users')
      ->where('id', CRUDBooster::myId())
      ->select('password')
      ->get();

      $validator = $request->validate([
          'passwordLama' => 'required',
          'passwordBaru' => 'required',
          'confirmasiPasswordBaru' =>['same:passwordBaru']
      ]);

      // dd($request->passwordLama, $oldPassword[0]->password);
      if(\Hash::check($request->passwordLama, $oldPassword[0]->password)){
          DB::table('cms_users')
          ->where('id', CRUDBooster::myId())
          ->update(['password' => \Hash::make($request->passwordBaru)]);
      }
      
      return redirect()->back();
  }
  public function help(){
      return view('webpages.sub_pages_setting.help');
  }
  public function helpManageAccount(){
      return view('webpages.sub_pages_setting.sub_pages_help.manage-account');
  }
  public function paymentMethod(){
      return view('webpages.sub_pages_setting.sub_pages_help.payment-method');
  }
  public function installRobot(){
      return view('webpages.sub_pages_setting.sub_pages_help.install-robot');
  }
  public function manageVPS(){
      return view('webpages.sub_pages_setting.sub_pages_help.manage-vps');
  }
  public function complain(){

      // check ticket status
      $cekTicketStatus = DB::table('report')
      ->where('userId', CRUDBooster::myId())
      ->leftjoin('tickets', 'report.ticketId', 'tickets.id')
      ->where('tickets.status', 1)
      ->first();
      // dd($cekTicketStatus);
      if($cekTicketStatus){
          // get data
          $chatData = DB::table('report')
          ->where('ticketId', $cekTicketStatus->ticketId)
          ->leftjoin('cms_users', 'report.userId', 'cms_users.id')
          ->orderBy('report.created_at')
          ->get();
          $ticketId = $cekTicketStatus->ticketId;
          // dd($chatData);
          return view('webpages.sub_pages_setting.complain-chat', compact('ticketId', 'chatData'));
      }else{
           // create the ticket
        $ticketRandomString = bin2hex(random_bytes(15));
        $ticket = $request->product . $ticketRandomString . CRUDBooster::myId();

        DB::table('tickets')
        ->insert(['ticketNumber' => $ticket, 'status' => false, 'created_at' => date('Y-m-d H:i:s')]);

        return view('webpages.sub_pages_setting.complain', compact('ticket'));
      }
  }

  public function complainContent(Request $request, $ticket){
      $validator = $request->validate([
          'complain' => 'required',
      ]);

      // update ticket status
      DB::table('tickets')
      ->where('ticketNumber', $ticket)
      ->update(['status'=> true]);

      if($request->hasFile('photo')){
          $validator = $request->validate([
          'photo' => 'image|mimes:jpeg,png,jpg'
          ]);

          // get ticketId
          $ticket = DB::table('tickets')
          ->where('ticketNumber', $ticket)
          ->first();
          
          date_default_timezone_set("Asia/Jakarta");
          $file = $request->file('photo');
          // uniquefilename yang dimasukkan ke database
          $fileName = $request->file('photo')->getClientOriginalName();
          $uniqueFileName= str_replace(" ","",time()."_".$fileName);
          
          $filePath = 'uploads/'.CRUDBooster::myId();
          
          $fileNameToStore = $filePath.'/'.$uniqueFileName;
          $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);
          // dd($fileNameToStore);
          // insert into report
          DB::table('report')
          ->insert(['ticketId' => $ticket->id, 'userId' => CRUDBooster::myId(), 'chat' => $request->complain, 'photoComplain' => $fileNameToStore, 'created_at' => date('Y-m-d H:i:s')]);
          return redirect()->route('complainContentView');
          // return view('web_pages.pages.sub_pages_setting.complain-chat', compact('ticket', 'chatData'));

      }else{
          // get ticketId
          $ticketData = DB::table('tickets')
          ->where('ticketNumber', $ticket)
          ->first();

          // insert into report
          DB::table('report')
          ->insert(['ticketId' => $ticketData->id, 'userId' => CRUDBooster::myId(), 'chat' => $request->complain, 'created_at' => date('Y-m-d H:i:s')]);

          // get data
          $chatData = DB::table('report')
          ->where('ticketId', $ticketData->id)
          ->leftjoin('cms_users', 'report.userId', 'cms_users.id')
          ->orderBy('report.created_at')
          ->get();
          return redirect()->route('complainContentView');

      }
  }

  public function complainContentView(){

      $chatData = DB::table('report')
      ->where('userId', CRUDBooster::myId())
      ->leftjoin('tickets', 'tickets.id', 'report.ticketId')
      ->leftjoin('cms_users', 'cms_users.id', 'report.userId')
      ->where('tickets.status', 1)
      ->get();
      $ticketId = $chatData[0]->ticketId;
      // dd($chatData, $ticketId);
      return view('webpages.sub_pages_setting.complain-chat', compact('ticketId', 'chatData'));

  }

  public function reportButton($id){
      $data = DB::table('tickets')
              ->where('id',$id)
              ->first();
              
      // dd($data, $id);
      if($data -> status == '1'){
          DB::table('tickets')
              ->where('id',$id)
              ->update(['status' => 0]);
      }
      if($data -> status == '0'){
          DB::table('tickets')
              ->where('id',$id)
              ->update(['status' => 1]);
      }
      return redirect()->back();
  }

  public function reportDetail($id){
      $chatData = DB::table('report')
      ->where('ticketId', $id)
      ->leftjoin('tickets', 'tickets.id', 'report.ticketId')
      ->leftjoin('cms_users', 'cms_users.id', 'report.userId')
      ->get();

      $ticketId = $chatData[0]->ticketId;
      // dd($ticketId);
      return view('webpages.sub_pages_setting.complain-chat', compact('ticketId', 'chatData'));
  }

  public function paymentVerification($id){
      $invoice="ENV-".time()."-".CRUDBooster::myID();
      DB::table('sales')
      ->where('id', $id)
      ->update([
        'salesPaymentStatus' => 'paid',
        'invoice'=>$invoice
        ]);
      

      return redirect('/admin/orders29');
  }

  public function printReceipt($id){
      $salesData = DB::table('sales')
      ->where('sales.id', $id)
      ->leftjoin('cms_users', 'cms_users.id', 'sales.user_id')
      ->select('sales.id', 'cms_users.name', 'cms_users.lastName', 'sales.salesTime', 'sales.salesSart', 'sales.salesSubtotal', 'sales.salesDiscount', 'sales.salesTotal', 'sales.invoice')
      ->first();

      // $productDetails = unserialize($salesData->salesSart);
      // dd($productDetails);

      // dd($salesData);
      return view('webpages.admin.print-invoice', compact('salesData'));
  }
  
  
  public function postChat(Request $request){
      DB::table('report')
      ->insert(['ticketId' => $request->input('ticketId'), 'userId' => CRUDBooster::myID(),'chat' => $request->input('chatContent')]);
  
      $chatData = DB::table('report') 
      ->where('report.ticketId', $request->input('ticketId'))
      ->leftjoin('cms_users','report.userId', 'cms_users.id')
      ->select('cms_users.id_cms_privileges', 'report.chat', 'report.created_at')
      ->orderBy('report.id','desc')
      ->first();
      
      return response()->json(['chatData'=>$chatData, 'ticketId'=>$request->input('ticketId')]);
  }


  // CONTROLLER LAMA
    public function updateAccount(Request $request) {
      $validator = $request->validate([
        'name' => 'required|string|min:3',
        'phoneNumber' => 'required|numeric',
        'address' => 'required|string',
        'zipCode' => 'required|numeric',
        'lastName' => 'string'
      ]);
      // dd($request->hasFile('photo'));

      if($request->hasFile('photo')){
        $validator = $request->validate([
          'photo' => 'image|mimes:jpeg,png,jpg'
        ]);

        date_default_timezone_set("Asia/Jakarta");
				$file = $request->file('photo');
        // uniquefilename yang dimasukkan ke database
        $fileName = $request->file('photo')->getClientOriginalName();
        $uniqueFileName= str_replace(" ","",time()."_".$fileName);
        
        $filePath = 'uploads/'.CRUDBooster::myId();
        
        $fileNameToStore = $filePath.'/'.$uniqueFileName;

        $tes = Storage::putFileAs($filePath, $file, $uniqueFileName);
        // $files = Storage::allFiles('uploads/'.$uniqueFileName);
				// $request['photo'] = $uniqueFileName;
        
        DB::table('cms_users')
        ->where('id', CRUDBooster::myId())
        ->update([
          'name' => $request->name,
          'phoneNumber' => $request->phoneNumber,
          'address' => $request->address,
          'zipCode' => $request->zipCode,
          'lastName' => $request->lastName,
          'photo' => $fileNameToStore
          ]);
        return redirect()->back();

      }else{
        // dd('tes');
        DB::table('cms_users')
        ->where('id', CRUDBooster::myId())
        ->update([
          'name' => $request->name,
          'phoneNumber' => $request->phoneNumber,
          'address' => $request->address,
          'zipCode' => $request->zipCode,
          'lastName' => $request->lastName
        ]);
        return redirect()->back();

      }
      // dd($request);
  }

  public function userHome(){
    return view('custom_dashboard.userHome');
  }

  public function submitReport(Request $request, $id){
    $validator = $request->validate([
      'product' => 'required',
      'selectCategory' => 'required',
      'complain' => 'required',
      'title' => 'required',
      
    ]);

    // create the ticket
    $ticketRandomString = bin2hex(random_bytes(15));
    $ticket = $request->product . $ticketRandomString . CRUDBooster::myId();
    
    // get ticket category
    $ticketCategories = DB::table('categorytickets')
          ->where('nmCategoryTickets', $request->selectCategory)
          ->get();
    // dd($ticketCategories[0]->id, $id, $ticket, $request->title);

    // insert data into tickets table
    DB::table('tickets')->insert(
      ['categoryTicketsId' => $ticketCategories[0]->id, 'orderId' => $id, 'ticketNumber' => $ticket, 'subjek' => $request->title, 'status' => true, 'created_at' => date('Y-m-d H:i:s')]
    );

    // get data in tickets
    $data = DB::table('tickets')
    ->where('ticketNumber', $ticket)
    ->get();
    // dd($data);

    // insert into ticketconversations
    DB::table('ticketconversations')
    ->insert(
      ['ticketId' => $data[0]->id, 'userId' => CRUDBooster::myId(), 'chat' => $request->complain, 'created_at' => date('Y-m-d H:i:s')]
    );

    return redirect()->back();
  }

  public function userLogout(){
    Session::flush();
    return redirect()->route('homePage');
  }

}
