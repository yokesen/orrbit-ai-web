<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DB;
use Cart;
use Storage;
use Session;
use CRUDbooster;
class PageController extends Controller
{
    public function pageIndex()
    {

        $title = "Orrbit - Overnight Arbitrage";
        $description = "Swap arbitrage trades Swap Rates against each other. All Positions are at all times hedged against each other and there is no directional trading risk involved";
        $imageog = url('/') . "/webpages/images/LogoYokosenPojokKanan.png";

        $robots=DB::table('robots')->first();

        $client = new Client();
        $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
        $currency_data = json_decode($res->getBody());
        $currency = $currency_data->rates;

        $locale = \App::getLocale();
        // dd($locale);
        // dd($matauang);
        // dd($robots);

        // $robots=DB::table('robots')->leftjoin('robot_details','robot_details.robotId','robots.id')->get();
        return view('webpages.homepage', compact('title', 'description', 'imageog', 'robots', 'currency', 'locale'));
    }

    public function pagePricing()
    {
        $robots=DB::table('robots')->first();
        $client = new Client();
        $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
        $currency_data = json_decode($res->getBody());
        $currency = $currency_data->rates;
        $locale = \App::getLocale();
        // dd($robots);
        return view('webpages.pricing',compact('robots', 'currency', 'locale'));
    }

    public function pageSwap()
    {
        return view('webpages.swap');
    }

    public function pageTechnology()
    {
        return view('webpages.technology');
    }

    public function pageBlog()
    {
        return view('webpages.blog');
    }

    public function pageArticle()
    {
        return view('webpages.article');
    }

    public function pageRegister()
    {
        return view('webpages.register');
    }


    public function pageCart()
    {
        // dd(Cart::content());
        $jasa = DB::table('jasa')->get();
        $client = new Client();
        $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
        $currency_data = json_decode($res->getBody());
        $currency = $currency_data->rates;
        $locale = \App::getLocale();
        return view('webpages.cart', compact('jasa', 'currency', 'locale'));
    }

    public function pageHistory()
    {
        $jasa = DB::table('jasa')->get();
        $checkout = DB::table('sales')->where('salesPaymentStatus','!=','menunggu')->where('user_id',CRUDBooster::myId())->get();
        return view('webpages.history',compact('jasa','checkout'));
    }

    public function pageCoupon()
    {
        $voucher = DB::table('vouchers')
        ->where('expireVoucher','>',date('Y-m-d H:i:s'))
        ->where('sisaVoucher','>',0)
        ->get();
        return view('webpages.coupon',compact('voucher'));
    }

    public function pageCollection()
    {
        $jasa = DB::table('jasa')->get();
        $checkout = DB::table('sales')->where('salesPaymentStatus','!=','menunggu')->where('user_id',CRUDBooster::myId())->get();
        $unpaid = DB::table('sales')->where('salesPaymentStatus','menunggu')->where('user_id',CRUDBooster::myId())->get();
      //   $checkout = DB::table('sales')->where('salesStatus','!=','menunggu pembayaran')->where('user_id',CRUDBooster::myId())->get();
        $robots = DB::table('robots')->get();
        return view('webpages.collection',compact('jasa','checkout','unpaid'));
    }
}
