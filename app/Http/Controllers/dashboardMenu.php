<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Alert;
use CRUDBooster;
use Storage;
use Session;

use App\Http\Controllers\Controller;


class dashboardMenu extends \crocodicstudio\crudbooster\controllers\CBController
{
    // menu
    // public function userHelp() {
    //     return view('custom_dashboard.user_help');

    // }
    
    // public function store() {
    //     $getProducts = DB::table('products')
    //     ->leftJoin('product_periods','product_periods.idProduct','products.id')
    //     ->select('product_periods.periodMonth','product_periods.periodPrice', 'product_periods.id','products.productName', 'products.pictures')
    //     ->get();

    //     $getUser = DB::table('cms_users')
    //     ->where('id', CRUDBooster::myId())
    //     ->get();

    //     // dd($getProducts[0]->productName);
    //     return view('custom_dashboard.store', compact('getProducts', 'getUser'));

    // }

    // public function storePaymentStep1($id){
    //     $robots = DB::table('product_periods')
    //     ->where('product_periods.id',$id)
    //     ->leftJoin('products','product_periods.idProduct','products.id')
    //     ->select( 'product_periods.id','product_periods.periodMonth','product_periods.periodPrice','products.productName', 'products.pictures')
    //     ->get();
    //     // dd($robots);

    //     return view('custom_dashboard.storePaymentStep2', compact('robots'));
    // }

    // public function storePaymentStep2($id){
    //     $robots = DB::table('product_periods')
    //     ->where('product_periods.id',$id)
    //     ->leftJoin('products','product_periods.idProduct','products.id')
    //     ->select('product_periods.id','products.productName', 'products.pictures')
    //     ->get();

    //     $user = DB::table('cms_users')
    //     ->where('cms_users.id', CRUDBooster::myId())
    //     ->get();
    //     // dd($user);

    //     return view('custom_dashboard.storePaymentStep3', compact('robots'));
    // }

    // public function account() {
    //     $id = CRUDBooster::myId();
    //     $userData = DB::table('cms_users')
    //     ->where('id', $id)
    //     ->first();
    //     // dd($userData);
    //     return view('custom_dashboard.account', compact('id', 'userData'));

    // }

    // public function updateAccount(Request $request) {
    //     $firstName = request()->firstName;
    //     $lastName = request()->lastName;
    //     $phoneNumber = request()->phoneNumber;
    //     $address = request()->address;
    //     $zipCode = request()->zipCode;
    //     $data = [];
    //     $data[] = ['firstName'=>$firstName, 'lastName'=>$lastName,'phoneNumber'=>$phoneNumber,'address'=>$address, 'zipCode'=>$zipCode];
    //     $validator = $data->validate([
    //         'firstName' => 'required|string|min:3'
    //     ]);
        
    // }
    // Admin button
    // public function paymentVerification($id) {
        
    //     // ambil isi data orders dulu berdasarkan where
    //     $getData = DB::table('orders')
    //     ->where('orders.id',$id)
    //     ->leftJoin('cms_users','orders.userId','cms_users.id')
    //     ->leftJoin('products','orders.productId','products.id')
    //     ->leftJoin('product_periods', 'orders.idProductPeriods', 'product_periods.id')
    //     ->select('orders.created_at','orders.id','orders.struck','orders.payment', 'cms_users.name','cms_users.lastName', 'products.productName','product_periods.periodMonth')
    //     ->get();
        
    //     return view('custom_dashboard.payment_verification', compact('getData'));
    // }

    // public function paymentConfirmation(Request $request){
    //     $updateData = DB::table('orders')
    //     ->where('id',request()->id)
    //     ->update(['payment'=>1]);

    //     $getData = DB::table('orders')
    //     ->where('id',request()->id)
    //     ->get();

    //     // dd($getData);
    //     return redirect()->back()->with('getData');
    // }

    // // User Button
    // public function printReceipt($id){
    //     $getData = DB::table('orders')
    //     ->where('orders.id',$id)
    //     ->leftJoin('cms_users','orders.userId','cms_users.id')
    //     ->leftJoin('products','orders.productId','products.id')
    //     ->leftJoin('product_periods', 'orders.idProductPeriods', 'product_periods.id')
    //     ->select('orders.created_at','orders.id','orders.struck', 'cms_users.name','cms_users.lastName', 'products.productName','product_periods.periodMonth')
    //     ->get();

    //     return view('custom_dashboard.print_invoice', compact('getData'));
    // }

    public function adminHome(){
        return view('custom_dashboard.admin_home');
    }

    // public function userReport(){
    //     return view('custom_dashboard.report');
    // }

    // public function userReportId($id){
    //     // get data
    //     $getData = DB::table('orders')
    //     ->where('orders.id', $id)
    //     ->leftJoin('products','orders.productId','products.id')
    //     ->select('products.productName')
    //     ->get();

    //     // get ticket category
    //     $ticketCategories = DB::table('categorytickets')
    //     ->get();
    //     // dd($getData, $id, $ticketCategories);
    //     return view('custom_dashboard.report', compact('getData','id', 'ticketCategories'));
    // }

    public function reportButton($id){
        $data = DB::table('tickets')
                ->where('id',$id)
                ->first();
                
        // dd($data, $id);
        if($data -> status == '1'){
            DB::table('tickets')
                ->where('id',$id)
                ->update(['status' => 0]);
        }
        if($data -> status == '0'){
            DB::table('tickets')
                ->where('id',$id)
                ->update(['status' => 1]);
        }
        return redirect()->back();

    }

    // public function reportDetail($id){
    //     $data = DB::table('ticketconversations')
    //     ->where('ticketId', $id)
    //     // ->leftjoin()
    //     ->get();

    //     return view('custom_dashboard.reportDetail');
    // }

    // public function tesButton($id){
    //     dd($id);
    // }

    
}