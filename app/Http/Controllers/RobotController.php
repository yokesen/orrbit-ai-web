<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RobotController extends Controller
{
  public function futureSpot(Request $request){

      if($request['method'] == "initiate"){

          $check =  DB::table('traders')->where('licenseKey',$request['license'])->count();
          $license = DB::table('traders')->where('licenseKey',$request['license'])->first();
          $status = $license->status;
          $server = $request['server'];
          $account = $request['account'];
          $pair = $request['pair'];

          if($check > 0){

            if($status == 'Waiting'){
              $update = DB::table('traders')->where('licenseKey',$request['license'])->update([
                'broker' => $request['broker'],
                'server' => $request['server'],
                'account' => $request['account'],
                'pair' => $request['pair'],
                'status' => 'Active'
              ]);

              $return = "Server $server and Account $account updated. Now you are good to go to arbitrage on $pair";
            }elseif($status == 'Active'){
              $usage = DB::table('traders')->where([
                'licenseKey' => $request['license'],
                'server' => $request['server'],
                'account' => $request['account'],
                'pair' => $request['pair'],
                'status' => 'Active'
              ])->count();

              if($usage == 1){
                $return = "Your license is ACTIVE";
              }else{
                $return = "Your license is ALREADY USED on server $license->server with account $license->account on pair $license->pair please buy another license";
              }
            }elseif($status == 'Suspended'){
              $return = "Your license is SUSPENDED";
            }elseif($status == 'Expired'){
              $return = "Your license is EXPIRED please pay your SUBSCRIPTION";
            }

          }else{
            $return = "Your license key is INVALID";
          }

          return $return;

      }elseif($request['method'] == "cantrade"){

        $usage = DB::table('traders')->where([
          'licenseKey' => $request['license'],
          'server' => $request['server'],
          'account' => $request['account'],
          'pair' => $request['pair'],
          'status' => 'Active'
        ])->count();

        if($usage == 1){
          $return = "Active";
        }else{
          $return = "Not-Valid-Server-Account-And-Pair";
        }
        return $return;
      }
    }
}
