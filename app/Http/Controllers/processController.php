<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Cart;
use Storage;
use Session;
use DB;
use CRUDBooster;
class processController extends Controller
{
    //

    public function addToCart($id, $month){
        Cart::destroy();
        $cart = DB::table('robots')->where('id',$id)->first();
        $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
        if($month==1){
            $price=$cart->robotPrice3;
        }
        elseif($month==3){
            $price=$cart->robotPrice2;
        }
        elseif($month==12){
            $price=$cart->robotPrice1;
        }
        foreach ($jasa as $service) {
            $additional[$service->jasaName] = 0;
        }
        $promo = [
          'voucher' => 'none',
          'amount' => 0
        ];

        $periode = [
          'kali' => $month,
          'sebutan' => $month.' bulan',
          'harga' => $price
        ];
        // dd($periode);
        Cart::add($cart->id, $cart, 1, $cart->robotPrice1, ['periode' => $periode, 'service' => $additional,'promo' => $promo]);
        return redirect()->route('cartPage');
    }

    public function useVoucher($kode){
        if(Cart::content()->isNotEmpty()){
            foreach (Cart::content() as $cart) {
              $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
        
              $service = $cart->options->service;
              $promo = $cart->options->promo;
              $periode = $cart->options->periode;
        
              $voucher = DB::table('vouchers')->where('kodeVoucher',$kode)
                        ->where('expireVoucher','>',date('Y-m-d H:i:s'))
                        ->where('sisaVoucher','>',0)
                        ->first();
        
              $promo = [
                'voucher' => $kode,
                'amount' => $voucher->diskonVoucher
              ];
              $ganti = ['periode'=>$periode,'service' => $service,'promo' => $promo];
        
              $next = Cart::update($cart->rowId,['options' => $ganti]);
            }
        }
        return redirect()->route('cartPage');
    }

    public function buyAgain($id){
        $checkout = DB::table('sales')
        ->where('user_id',CRUDBooster::myId())
        ->where('salesStatus','menunggu pembayaran')
        ->where('id',$id)
        ->orderby('id','desc')
        ->first();
        $robot = unserialize($checkout->salesSart);
        foreach($robot as $robots){
          // dd($robots->name->robotSlugName);
          $cart = DB::table('robots')->where('robotSlugName',$robots->name->robotSlugName)->first();
          $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
          foreach ($jasa as $service) {
              $additional[$service->jasaName] = 0;
          }
          $promo = [
            'voucher' => 'none',
            'amount' => 0
          ];
          $periode = [
            'kali' => 1,
            'sebutan' => '1 bulan',
            'harga' => $cart->robotPrice1
          ];
          Cart::add($cart->id, $cart, 1, $cart->robotPrice1, ['periode' => $periode, 'service' => $additional,'promo' => $promo]);
        }
        return redirect()->route('cartPage');
      }

      public function shopingCart(){
        $jasa = DB::table('shoppingcart')->first();
        $jasa=unserialize($jasa->content);
        return response()->json($jasa);
      }

      public function processCheckout(){
        
        if(Cart::count()>0){
          foreach (Cart::content() as $n => $cart){
            $biayaService = 0;
            $jasa = DB::table('jasa')->get();
            foreach ($jasa as $service){
              if($cart->options->service[$service->jasaName] != 0){
                $biayaService += $cart->options->service[$service->jasaName];
              }
            }
            $totalService += $biayaService;
            $totalRobot += $cart->price;
            if ($cart->options->promo['amount']>0) {
              $potongan = $cart->options->promo['amount'];
              $kodenya = $cart->options->promo['voucher'];
            }
          }
          $subtotal = $totalRobot + $totalService;
          $diskon = $potongan * $totalRobot * 0.01;
          $total = $subtotal - $diskon;
    
          $insert = DB::table('sales')->insertGetId([
            'salesCode' => 'SW-'.CRUDBooster::myID().'-'.time(),
            'user_id' => CRUDBooster::myID(),
            'salesSart' => serialize(Cart::content()) ,
            'salesSubtotal' => $subtotal,
            'salesDiscount' => $diskon,
            'salesVoucher' => $kodenya,
            'salesTotal' => $total,
            'salesStatus' => 'menunggu pembayaran',
            'salesTime' => date('Y-m-d H:i:s'),
            'salesPaymentStatus' => 'menunggu',
            'salesDownloadStatus' => 'menunggu'
          ]);
    
          Cart::destroy();
          return redirect()->route('checkout');
        }else{
          return redirect()->route('dashboard');
        }
      }

      public function removeFromCart($rowId){

        Cart::remove($rowId);
        return redirect()->back();
      }

      public function rentPeriode($requestService,$rowId){
        $cart = Cart::get($rowId);
        $robot = DB::table('robots')->where('id',$cart->id)->first();

        $client = new Client();
        $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
        $currency_data = json_decode($res->getBody());
        $currency = $currency_data->rates;
        $locale = \App::getLocale();
        
        switch ($requestService) {
          case '1': $price = $robot->robotPrice3;$sebutan= '1 bulan';$kali = 1;break;
          case '3': $price = $robot->robotPrice2;$sebutan= '3 bulan';$kali = 3;break;
          case '12': $price = $robot->robotPrice1;$sebutan= '1 tahun';$kali = 12;break;
    
          default:
            $price = $robot->robotPrice3;
            $sebutan = '1 bulan';
            $kali = 1;
            break;
        }
    
        $promo = $cart->options->promo;
    
        $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
        $additional = $cart->options->service;
        foreach ($jasa as $service) {
          if($service->jasaPeriode != "monthly"){
            $multi = 1;
          }else{
            $multi = $kali;
          }
          if ($additional[$service->jasaName] != 0) {
            $additional[$service->jasaName] = $service->jasaPrice * $multi;
          }
    
        }
    
        $periode = [
          'kali' => $kali,
          'sebutan' => $sebutan,
          'harga' => $price
        ];
    
        $ganti = ['periode'=>$periode,'service' => $additional,'promo' => $promo];
        //dd($ganti);
        $next = Cart::update($rowId,['price' => $price, 'options' => $ganti]);
        $view = view('webpages.ajax.cart-ajax',compact('jasa', 'currency', 'locale'))->render();
    
        return response()->json(['html'=>$view]);
      }

      public function addService($requestService,$rowId){
        //dd(Cart::content(),Cart::get($rowId));
        $cart = Cart::get($rowId);
        $existing = $cart->options;
        $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
        $additional = $cart->options->service;
        $promo = $cart->options->promo;
        $periode = $cart->options->periode;

        $client = new Client();
        $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
        $currency_data = json_decode($res->getBody());
        $currency = $currency_data->rates;
        $locale = \App::getLocale();
    
        foreach ($jasa as $service) {
          if($service->jasaPeriode == "monthly"){
            $kali = $periode['kali'];
          }else{
            $kali = 1;
          }
          if($service->jasaName == $requestService){
            $additional[$service->jasaName] = $service->jasaPrice * $kali;
          }
        }
    
        $ganti = ['periode' => $periode, 'service' => $additional,'promo' => $promo];
    
        $next = Cart::update($rowId,['options' => $ganti]);
    
        $view = view('webpages.ajax.cart-ajax',compact('jasa', 'locale', 'currency'))->render();
    
        //dd(Cart::content(),$view);
    
        return response()->json(['html'=>$view]);
    
      }

      public function removeService($requestService,$rowId){
        //dd(Cart::content(),Cart::get($rowId));
        $cart = Cart::get($rowId);
        $existing = $cart->options;
        $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
        $additional = $cart->options->service;
        $promo = $cart->options->promo;
        $periode = $cart->options->periode;
    
        $client = new Client();
        $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
        $currency_data = json_decode($res->getBody());
        $currency = $currency_data->rates;
        $locale = \App::getLocale();

        foreach ($jasa as $service) {
          if($service->jasaName == $requestService){
            $additional[$service->jasaName] = 0;
          }
        }
    
        $ganti = ['periode' => $periode,'service' => $additional,'promo' => $promo];
    
        $next = Cart::update($rowId,['options' => $ganti]);
    
        $view = view('webpages.ajax.cart-ajax',compact('jasa', 'locale', 'currency'))->render();
    
        return response()->json(['html'=>$view]);
    
      }

      public function kodeVoucher($kode){
        foreach (Cart::content() as $cart) {
          $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
    
          $service = $cart->options->service;
          $promo = $cart->options->promo;
          $periode = $cart->options->periode;

          $client = new Client();
          $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
          $currency_data = json_decode($res->getBody());
          $currency = $currency_data->rates;
          $locale = \App::getLocale();
    
          $voucher = DB::table('vouchers')->where('kodeVoucher',$kode)
                    ->where('expireVoucher','>',date('Y-m-d H:i:s'))
                    ->where('sisaVoucher','>',0)
                    ->first();
    
          $promo = [
            'voucher' => $kode,
            'amount' => $voucher->diskonVoucher
          ];
          $ganti = ['periode'=>$periode,'service' => $service,'promo' => $promo];
    
          $next = Cart::update($cart->rowId,['options' => $ganti]);
          $view = view('webpages.ajax.cart-ajax',compact('jasa', 'locale', 'currency'))->render();
    
          return response()->json(['html'=>$view]);
        }
      }

      public function removeVoucher(){
        foreach (Cart::content() as $cart) {
          $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
    
          $service = $cart->options->service;
          $promo = $cart->options->promo;
          $periode = $cart->options->periode;

          $client = new Client();
          $res = $client->get('https://api.exchangeratesapi.io/latest?base=USD');
          $currency_data = json_decode($res->getBody());
          $currency = $currency_data->rates;
          $locale = \App::getLocale();
    
          $promo = [
            'voucher' => 'none',
            'amount' => 0
          ];
          $ganti = ['periode'=>$periode,'service' => $service,'promo' => $promo];
    
          $next = Cart::update($cart->rowId,['options' => $ganti]);
          $view = view('webpages.ajax.cart-ajax',compact('jasa', 'locale', 'currency'))->render();
    
          return response()->json(['html'=>$view]);
        }
      }

      public function getModalData(){
        $data=Cart::content()->sortByDesc('id');
        $jasa = DB::table('jasa')->where('jasaStatus','active')->get();
        $view = view('webpages.ajax.modal-checkout',compact('data','jasa'))->render();
        return response()->json(['html'=>$view]);
      }

}
