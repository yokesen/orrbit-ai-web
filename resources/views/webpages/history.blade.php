@extends('webpages.templates.master')
@section('title','History | Overnight Arbitrage')

@section('content')
    <div class="container">
        <h2 class="my-5">Riwayat Pembelian</h2>
  @forelse ($checkout as $value)
        <?php
          $cart = unserialize($value->salesSart);
          $totalProduct=count($cart);
          $j=1;
          // dd($value->id);
        ?>
        <div class="cart-block mt-3 mb-5 p-3">
            <div class="row">
                <div class="col-md-12">
                   <p>{{date('d M Y', strtotime($value->salesTime))}} - {{$value->salesCode}}</p>
                </div>
            </div>
           {{-- Foreach --}}
           @foreach ($cart as $n => $check)
            <div class="row align-center">
                <div class="col-md-2">
                  <img src="{{env('APP_IMG')."/".$check->name->robotImage}}" alt="Gambar robot" class="img-fluid">
                </div>
                <div class="col-md-5 align-self-center">
                    <p  style="margin-bottom: 0px">{{$check->name->robotLongName}}</p>
                    <p class="font-weight-bold"  style="margin-bottom: 0px">${{number_format($check->options->periode['harga'],'0','.','.')}}/{{$check->options->periode['sebutan']}}</p>
                    @if($totalProduct>1)
                    <p><small>(+{{$totalProduct-1}} produk lainnya)</small></p>
                    @endif
                  </div>
                <div class="col-md-3 align-self-center">
                    <p style="margin-bottom: 0px">Total Harga</p>
                    <p class="font-weight-bold">${{number_format($value->salesTotal,'0','.','.')}}</p>
                </div>
                <div class="col-md-2 align-self-center">
                    <div>
                    <button class="btn beli-lagi" onclick="window.location.href='{{ route('buyAgain', ['id' => $value->id]) }}'">Beli lagi</button>
                    </div>
                </div>
            </div>
            @break
            @endforeach
                <a href="#" class="detail-history" data-toggle="modal" data-target="#modalDetailPesanan{{$value->id}}">Lihat detail pesanan</a>
        </div>
  @empty
        <div><h3>Belum Melakukan Pembelian</h3></div>
  @endforelse
    </div>



    {{-- Modal Detail Pesanan --}}
    @foreach ($checkout as $value)
        <?php
          $cart = unserialize($value->salesSart);
          $totalProduct=count($cart);
          $j=1;
          // dd($value->id);
        ?>
    <div class="modal fade" id="modalDetailPesanan{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="modalDetailPesanan{{$value->id}}" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: transparent">
    
                <div class="row" style="row-gap: 10px">
                    <div class="col-md-3">Dari: Admin, Inc.</div>
                    <div class="col-md-4"> Kepada: {{CRUDBooster::myName()}} </div>
                    <div class="col-md-5">Invoice  {{$value->salesCode}} <br> Tanggal Pembelian: {{date('d M Y', strtotime($value->salesTime))}}</div>
                </div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                {{-- Table --}}
    
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Produk</th>
                            <th scope="col">Periode</th>
                            <th scope="col">License Key</th>
                            <th scope="col">Deskripsi</th>
                            <th scope="col">Harga</th>
                          </tr>
                    </thead>
                    <tbody>
                    @foreach ($cart as $n => $check)
                             <?php
                                $service="";
                                if($check->options['service']['install']!=0)
                                  $service.="install ";
                                if($check->options['service']['maintenance']!=0)
                                  $service.="maintenance ";
                                if($check->options['service']['vps']!=0)
                                  $service.="vps";
                
                             ?>
                      <tr>
                      <td>{{$check->name->robotLongName}}</td>
                      <td>{{$check->options->periode['sebutan']}}</td>
                      <td>{{$value->salesCode}}</td>
                      <td>{{$service}}</td>
                      <td>Rp. {{number_format($check->options->periode['harga'],'0','.','.')}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
    
                {{-- -------- --}}
            </div>
            <div class="modal-footer" style="border-top: transparent">
            <button type="button" class="btn beli-lagi" onclick="window.location.href='{{ route('buyAgain', ['id' => $value->id]) }}'">Beli lagi</button>
            </div>
          </div>
        </div>
      </div>

  @endforeach
@endsection