@extends('webpages.templates.master')
@section('title','Collection | Overnight Arbitrage')

@section('content')
<div class="container">
  <h2>My Robot</h2>

  <div class="card my-4" style="border-radius: 20px">
    <!-- navigation in .card-header -->
    <div class="card-header nav-produk" style="border-top-left-radius: 20px; border-top-right-radius: 20px">
      <ul class="nav nav-tabs nav-justified card-header-tabs">
        {{-- <ul class="nav nav-pills nav-justified card-header-pills"> --}}
        <li class="nav-item">
          <a class="nav-link active nav-produk-left" data-toggle="tab" href="#tab1">Daftar Robot yang dapat digunakan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link nav-produk-right" data-toggle="tab" href="#tab2">Daftar Produk Checkout</a>
        </li>
      </ul>
    </div>
    <!-- .card-body.tab-content  -->
    <div class="card-body tab-content">
      <div class="tab-pane fade show active" id="tab1">
        @php
        $i=1;
        @endphp
        @foreach ($checkout as $value)
        <?php
        $cart = unserialize($value->salesSart);
        ?>
        {{-- Produk yang dapat digunakan --}}
        <div class="cart-block mt-3 p-3">
          @foreach ($cart as $n => $check)
          <div class="row align-center">
            <div class="col-md-2">
              <img src="{{env('APP_IMG')."/".$check->name->robotImage}}" alt="Gambar robot" class="img-fluid">
            </div>
            <div class="col-md-5 align-self-center">
              <p style="margin-bottom: 0px">{{$check->name->robotLongName}}</p>
              <p class="font-weight-bold" style="margin-bottom: 0px">Expired Date.....</p>
            </div>
            <div class="col-md-3 align-self-center">
              <p style="margin-bottom: 0px">License key</p>
              <p class="font-weight-bold license-key" id="licenseKey{{$i}}">{{$value->salesCode}}</p>
              <button class="btn btn-sm license-btn">Copy Key</button>
            </div>
            <div class="col-md-2 align-self-center">
              <div class="input-group-append">
                <button class="btn btn-sm license-btn">Download Robot</button>
              </div>
            </div>
          </div>
          @php
          $i+=1;
          @endphp
          @endforeach
        </div>
        @endforeach

      </div>

      {{-- Daftar Produk Checkout --}}
      <div class="tab-pane fade" id="tab2">
        @foreach ($unpaid as $value)
        <?php
        $cart = unserialize($value->salesSart);
        $totalProduct = count($cart);
        $j = 1;
        // dd($value->id);
        ?>
        <div class="cart-block mt-3 mb-3 p-3">
          <div class="row">
            <div class="col-md-12">
              <p>{{date('d M Y', strtotime($value->salesTime))}}</p>
            </div>
          </div>
          {{-- Foreach --}}
          @foreach ($cart as $n => $check)
          <div class="row align-center">
            <div class="col-md-2">
              <img src="{{env('APP_IMG')."/".$check->name->robotImage}}" alt="Gambar robot" class="img-fluid">
            </div>
            <div class="col-md-5 align-self-center">
              <p style="margin-bottom: 0px">{{$check->name->robotLongName}}</p>
              <p class="font-weight-bold" style="margin-bottom: 0px">${{$check->options->periode['harga']}}/{{$check->options->periode['sebutan']}}</p>
              @if($totalProduct>1)
              <p><small>(+{{$totalProduct-1}} produk lainnya)</small></p>
              @endif
            </div>
            <div class="col-md-3 align-self-center">
              <p style="margin-bottom: 0px">Total Harga</p>
              <p class="font-weight-bold">${{$value->salesTotal}}</p>
            </div>
            <div class="col-md-2 align-self-center">
              <div>
                <button class="btn beli-lagi" data-toggle="modal" data-target="#modalDetailPesanan{{$value->id}}">Checkout</button>
              </div>
            </div>
          </div>
          @break
          @endforeach
        </div>
        @endforeach
      </div>
      {{-- ------------------------- --}}
    </div>
  </div>
</div>


{{-- Modal Detail Pesanan --}}
@foreach ($unpaid as $value)
<?php
$cart = unserialize($value->salesSart);
$totalProduct = count($cart);
$j = 1;
// dd($value->id);
?>
<div class="modal fade" id="modalDetailPesanan{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="modalDetailPesanan{{$value->id}}" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: transparent">

        <div class="row" style="row-gap: 10px">
          <div class="col-md-3">Dari: Admin, Inc.</div>
          <div class="col-md-4"> Kepada: {{CRUDBooster::myName()}} </div>
          <div class="col-md-5">Tanggal Pembelian: {{date('d M Y', strtotime($value->salesTime))}}</div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- Table --}}

        <table class="table table-borderless table-hover">
          <thead>
            <tr>
              <th scope="col">Produk</th>
              <th scope="col">Periode</th>
              <th scope="col">Deskripsi</th>
              <th scope="col">Harga</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($cart as $n => $check)
            <?php
            $service = "";
            if ($check->options['service']['install'] != 0)
              $service .= "install ";
            if ($check->options['service']['maintenance'] != 0)
              $service .= "maintenance ";
            if ($check->options['service']['vps'] != 0)
              $service .= "vps";

            ?>
            <tr>
              <td>{{$check->name->robotLongName}}</td>
              <td>{{$check->options->periode['sebutan']}}</td>
              <td></td>
              <td>${{$check->options->periode['harga']}}</td>
            </tr>

            <?php
            $biayaService = 0;
            ?>
            @foreach ($jasa as $service)
            @if ($check->options->service[$service->jasaName] != 0)
            <tr>
              <td></td>
              <td></td>
              <td>{{$service->jasaName}}</td>
              <td>+${{$check->options->service[$service->jasaName]}}</td>
            </tr>
            <?php
            $biayaService += $check->options->service[$service->jasaName];
            ?>
            @endif
            @endforeach
            <?php
            $totalService += $biayaService;
            $totalRobot += $check->price;

            if ($check->options->promo['amount'] > 0) {
              $potongan = $check->options->promo['amount'];
              $kodenya = $check->options->promo['voucher'];
            }
            ?>
            @endforeach
            <?php
            $subtotal = $totalRobot + $totalService;
            $diskon = $potongan * $totalRobot * 0.01;
            ?>
            <tr>
              <td></td>
              <td></td>
              <td>Subtotal</td>
              <td>${{$subtotal}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td>Discount</td>
              <td>-${{$diskon}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td><b>Total</b></td>
              <td><b>${{$subtotal-$diskon}}</b></td>
            </tr>
          </tbody>
        </table>

        {{-- -------- --}}
      </div>
      <div class="modal-footer" style="border-top: transparent">
        <button type="button" class="btn beli-lagi">Bayar Pesanan</button>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection