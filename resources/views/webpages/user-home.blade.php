@extends('crudbooster::admin_template')

@section('content')

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <img src="{{url('/')}}/webpages/images/user/home-banner.jpg" width="100%">
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  Checkout your product update
                </div>
                <div class="box-body">
                  {{date('d-m-Y H:i')}}
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header">
                  Share your referal key
                </div>
                <div class="box-body">
                  XXX-XXX
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              Weekly Report
            </div>
            <div class="box-body">

            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
