@extends('webpages.templates.master')

@section('title','Register | Overnight Arbitrage')

@section('content')
    <div class="container">
        <div class="register">
        <h1>Register</h1>
        
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        
        <form class="mt-5" method="post" action="{{route('processRegistering')}}">
            <div class="form-group" style="margin-bottom: 2em">
              <label for="exampleFullName">First Name</label>
              <input type="text" class="form-control" name="firstName" aria-describedby="firstName">
            </div>
            <div class="form-group" style="margin-bottom: 2em">
              <label for="exampleFullName">Last Name</label>
              <input type="text" class="form-control" name="lastName" aria-describedby="lastName">
            </div>
            <div class="form-group" style="margin-bottom: 2em">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" name="email" aria-describedby="emailHelp">
            </div>
            <div class="form-group" style="margin-bottom: 2em">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" name="password">
            </div>

            <div class="form-group" style="margin-bottom: 2em">
              <label for="exampleInputPassword1">Confirm Password</label>
              <input type="password" class="form-control" name="password_confirmation">
            </div>
            @csrf
            <button type="submit" class="btn btn-block">Register</button>
          </form>
        </div>
    </div>
@endsection
