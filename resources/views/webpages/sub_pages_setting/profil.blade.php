@extends('webpages.setting')
@section('title', 'Profile')
@section('setting')
  <div class="col-12 col-md-9 setting-content-wrapper">
    <div class="profile-form-wrapper">
      <form action="{{route('submitProfile')}}" method="post">
        @csrf
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Nama Depan</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="lastName" class="col-sm-2 col-form-label col-profil-label">Nama Belakang</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="lastName" name="lastName" value="{{$user[0]->lastName}}">
          </div>
        </div>
        <div class="form-group row profile-category">
          <label for="inputEmail3" class="col-sm-2 col-form-label col-profil-label">Email</label>
          <div class="col-sm-10">
            <input type="email" class="form-control user-input-profile" id="inputEmail3" disabled value="{{$user[0]->email}}">
          </div>
        </div>
        <div class="form-group row profile-category">
          <label for="phoneNumber" class="col-sm-2 col-form-label col-profil-label">Nomor HP</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="phoneNumber" name="phoneNumber" value="{{$user[0]->phoneNumber}}">
          </div>
        </div>
        <div class="form-group row profile-button-wrapper">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success profile-button">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection