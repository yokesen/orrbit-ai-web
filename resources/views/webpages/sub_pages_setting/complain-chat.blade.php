@extends('webpages.setting')
@section('title', 'Complain Chat')
@section('setting')
  <div class="col-12 col-md-9 setting-content-wrapper">
    <div class="chat-form-wrapper">

      @foreach ($chatData as $data)
          @if ($data->id_cms_privileges == 3)
            <div class="card card-content user-chat" style="width: 50%;">
              <div class="card-body">
                <p class="card-text" >{{$data->chat}}</p>
                @if ($data->photoComplain)
                <img class="img-complain-chat" id="user-chat-img" src="/{{$data->photoComplain}}" alt="">
                @endif
              </div>
            </div> 
          @elseif ($data->id_cms_privileges != 3)
            <div class="card card-content admin-chat" style="width: 50%;">
              <div class="card-body">
                <p class="card-text admin-chat-content" >{{$data->chat}}</p>
              </div>
            </div>           
          @endif
      @endforeach
            <div id="user-chat-data">

            </div>
            <div id="admin-chat-data">

            </div>
        <div class="form-group row ml-3 clear"> 
          <input type="text" id="input-chat" class="form-control mr-1 col-md-8 col-7" placeholder="">
          <button type="submit" class="btn col-md-2 col-3 btn-send-report" onclick="submitComplain()">Kirim</button>
        </div>
    </div>
  </div>
@endsection

@section('jsonpage')
  <script>
    function submitComplain(){
      // need to delete input chat
      var chatContent = document.getElementById("input-chat").value;
      var ticketId = "<?php echo $ticketId; ?>";
      var dataChat = {chatContent,ticketId}
      $.ajax({
              type:"POST",
              url:"/post-chat",
              data: dataChat,
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              } 
              })
              // e.preventdefault();
              .done(function(chatData)
              {
              // console.log(chatData.chatData);
                if(chatData.chatData.id_cms_privileges == 3){
                  $('#user-chat-data').append(
                    `
                    <div class="card card-content user-chat" id="user-chat-data" style="width: 50%;">
                      <div class="card-body">
                        <p class="card-text">${chatData.chatData.chat}</p>
                      </div>
                    </div>`
                  );
                }else if(chatData.chatData.id_cms_privileges != 3){
                  $('#admin-chat-data').append(
                    `
                    <div class="card card-content admin-chat" id="admin-chat-data" style="width: 50%;">
                      <div class="card-body">
                        <p class="card-text admin-chat-content">${chatData.chatData.chat}</p>
                      </div>
                    </div>`
                  );
                }
              })
              .fail(function(jqXHR, ajaxOptions, thrownError)
              {
                      alert('server not responding...');
              });
    } 
  </script>
@endsection