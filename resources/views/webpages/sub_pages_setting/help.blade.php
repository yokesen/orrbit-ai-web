@extends('webpages.setting')
@section('title', 'Help')
@section('setting')

  <div class="col-12 col-md-9 setting-content-wrapper">
    <div class="card setting-help-wrapper">
      <div class="card-body help-card-body">
        <a href="{{route('helpManageAccount')}}" class="setting-card-link card-menu-list">Pengaturan Akun</a>
        <a href="{{route('paymentMethod')}}" class="setting-card-link card-menu-list">Cara Pembayaran</a>
        <a href="{{route('installRobot')}}" class="setting-card-link card-menu-list">Cara Install Robot</a>
        <a href="{{route('manageVPS')}}" class="setting-card-link card-menu-list">Pengaturan VPS</a>
      </div>
    </div>
  </div>
    
@endsection