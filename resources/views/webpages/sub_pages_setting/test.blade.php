@extends('webpages.setting')
@section('title', 'Profile')
@section('setting')
  <div class="col-12 col-md-9 setting-content-wrapper">
    <div class="profile-form-wrapper">
      <form action="{{route('submitBlog')}}" method="post">
        @csrf
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Title</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category">
          <label for="phoneNumber" class="col-sm-2 col-form-label col-profil-label">Nomor HP</label>
          <div class="col-sm-10">
            <textarea id="konten" class="form-control" name="konten" rows="10" cols="50"></textarea>
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Call To Action</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Redirect</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Video</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Meta Description</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Meta Keywords</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Author</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Code</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Blog Status</label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-category" action="">
          <label for="name" class="col-sm-2 col-form-label col-profil-label">Release Date </label>
          <div class="col-sm-10">
            <input type="text" class="form-control user-input-profile" id="name" name="firstName" value="{{$user[0]->name}}">
          </div>
        </div>
        <div class="form-group row profile-button-wrapper">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success profile-button">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script>
    var konten = document.getElementById("konten");
      CKEDITOR.replace(konten,{
      language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
  </script>
@endsection