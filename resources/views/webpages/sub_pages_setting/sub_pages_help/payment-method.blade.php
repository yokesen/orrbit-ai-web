@extends('webpages.setting')
@section('title', 'Help Manage Account')
@section('setting')
<div class="col-12 col-md-9 setting-content-wrapper">
  <div class="card setting-help-wrapper">
    <div class="container-fluid sub-setting-content">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacus risus risus, ultrices pellentesque amet. Diam ullamcorper integer convallis ipsum, elementum quis sed tellus et. Eu amet, cursus at massa. Sit orci, nec, morbi pellentesque viverra. Sed ultricies habitant sapien commodo pellentesque rutrum metus. Diam velit sit nisl quam parturient ut sit cursus. Purus, vitae facilisi volutpat convallis porta donec.
        Dolor amet, sollicitudin diam ut dapibus scelerisque duis. Curabitur adipiscing ac nunc odio diam arcu, lacinia. 
    
        Elementum, semper sem euismod erat pellentesque scelerisque tempor. In massa nunc quisque morbi vehicula at enim. Felis pellentesque aliquet donec dolor pretium nunc libero. Consectetur magna massa congue elit tristique hac viverra mattis. Sed morbi cursus scelerisque ac non volutpat nulla. Cursus vitae interdum pellentesque viverra. Leo est elit mauris nunc. Leo nulla mattis nunc, ullamcorper semper ultricies. Laoreet nulla porta velit at faucibus arcu. Mattis vitae sit faucibus ipsum aliquam vestibulum dictum mauris sit. Fermentum ultrices dolor, semper venenatis, mi aenean. Eget ut vitae integer congue tortor tellus purus ac urna.
    
        Nulla sit molestie pharetra maecenas cum amet erat pretium tincidunt. Ut dui habitasse eu, tellus nunc et lacus massa leo. Semper eget quisque aenean nulla tortor. Vestibulum quis ultricies tortor aliquam eleifend. Etiam nibh viverra augue luctus aliquam. Odio nibh quam vel erat quam odio nunc consectetur nunc. Turpis odio enim ac sed sociis feugiat. Aliquet vitae orci curabitur montes, pulvinar consequat, aliquet euismod eget. Arcu mauris, velit sodales amet fermentum a volutpat. Aliquet justo, egestas eleifend a id. Massa neque tristique tincidunt lorem tristique eget et et. Viverra tempus amet facilisis ac aliquam habitasse vulputate. Proin non sagittis nulla arcu. Elementum tortor ut tortor, iaculis eget metus sed phasellus nunc.
    
        Sed amet lobortis quisque risus dictumst consectetur id. Risus lorem sed diam, urna, eget habitasse. Amet eu egestas bibendum sapien. Nullam ornare netus nulla elementum. Aenean nunc lacus sit in euismod ultrices. Amet elementum justo pulvinar accumsan, semper nisl blandit massa. Pulvinar est vulputate nisl purus, tincidunt justo. Penatibus bibendum fermentum, in nunc dolor lacinia. Sit in ut gravida risus, nam sit. Turpis aliquam eget maecenas molestie lectus venenatis auctor. Iaculis nulla eu lorem condimentum. Sed aliquet interdum pellentesque molestie enim viverra ac a natoque. Accumsan, pulvinar fermentum gravida sit eget augue. Id et sed ut velit eleifend. In morbi amet, est erat eget pulvinar id.
    
        Orci sit et tincidunt odio diam. Maecenas egestas eget a facilisi aliquet sit gr
    
        avida odio accumsan. Hendrerit nisi, ullamcorper nunc, ipsum feugiat massa dis aenean. Sed diam adipiscing urna feugiat. Non et eget egestas maecenas neque risus et enim in. Nisl massa lorem turpis congue. Nullam velit placerat pretium id at. Amet vulputate suspendisse vel cum egestas cursus in adipiscing lectus. Mauris tempor leo urna, in in tincidunt imperdiet. Sit magna vel, pulvinar risus at tempus. 
      </p>
      <p class="question">
        Apakah ini membantu menyelesaikan masalah anda?
      </p>

      <div class="button-manage-wrapper">
        <a href="#" class="btn btn-success btn-thumbs-up"><img class="help-thumbs" src="{{url('/')}}/dashboard/images/thumbs-up-solid 1.svg" alt=""></a>
        <a href="#" class="btn btn-danger btn-thumbs-down"><img class="help-thumbs" src="{{url('/')}}/dashboard/images/thumbs-down-solid 1.svg" alt=""></a>
      </div>
    </div>
  </div>
</div>
    
@endsection