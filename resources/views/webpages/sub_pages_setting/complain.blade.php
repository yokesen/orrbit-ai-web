@extends('webpages.setting')
@section('title', 'Complain')
@section('setting')
  <div class="col-12 col-md-9 setting-content-wrapper">
    <div class="profile-form-wrapper">

      <form action="{{route('complainContent', ['ticket'=> $ticket])}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group profile-category">
          <label for="complain" class="complain-label">Tuliskan laporan anda</label>
          <textarea class="form-control complain-text-area" id="complain" rows="6" name="complain"></textarea>
        </div>
        <div class="form-group profile-category">
          <label for="complain">Masukkan screenshot untuk bukti</label>
          <div class="complain-image-wrapper">
            <img class="complain-image" id="complain-image-id" src="{{ URL::asset('dashboard/images/default-complain-picture.svg') }}" alt="">
            <div class="upload-wrapper">
              <label class="upload-file-complain" for="my_file"><img src="{{ URL::asset('dashboard/images/plus.svg') }}" alt=""></label>
              <input name="photo" type="file" id="my_file" onchange="document.getElementById('complain-image-id').src = window.URL.createObjectURL(this.files[0])" style="display:none; visibility:hidden;" />
            </div>
          </div>
        </div>
        <div class="form-group row profile-button-wrapper">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success profile-button">Simpan</button>
          </div>
          
        </div>
      </form>
    </div>
  </div>
@endsection