@extends('webpages.setting')
@section('title', 'Change Password')
@section('setting')

<div class="col-12 col-md-9 setting-content-wrapper">
  <div class="profile-form-wrapper">
    <form class="profile-form" action="{{route('submitChangePassword')}}" method="post">
      @csrf
      <div class="form-group change-pass-form row profile-category" action="">
        <label for="Nama" class="col-sm-2 col-form-label col-profil-label">Password Lama</label>
        <div class="col-sm-10">
          <input type="password" class="form-control user-input-profile" id="nama" name="passwordLama">
          @if ($errors->first('passwordLama'))
            <div class="error-passwordLama form-control">
              <p class="text-danger">{{ $errors->first('passwordLama') }}</p>
            </div>   
          @endif
        </div>
      </div>
      <div class="form-group change-pass-form row profile-category" action="">
        <label for="Nama" class="col-sm-2 col-form-label col-profil-label">Pasword Baru</label>
        <div class="col-sm-10">
          <input type="password" class="form-control user-input-profile" id="nama" name="passwordBaru">
        </div>
      </div>
      <div class="form-group change-pass-form row profile-category" action="">
        <label for="Nama" class="col-sm-2 col-form-label col-profil-label">Konfirmasi Password Baru</label>
        <div class="col-sm-10">
          <input type="password" class="form-control user-input-profile" id="nama" name="confirmasiPasswordBaru">
          @if ($errors->first('confirmasiPasswordBaru'))
            <div class="error-passwordBaru form-control">
              <p class="text-danger">{{ $errors->first('confirmasiPasswordBaru') }}</p>
            </div>   
          @endif 
        </div>
      </div>
      <div class="form-group row profile-button-wrapper">
        <div class="col-sm-10">
          @if ($userData->providerOrigin == 'facebook' || $userData->providerOrigin == 'google')
            <button type="submit" class="btn btn-success profile-button pass-button" disabled>Simpan</button>
          @else
            <button type="submit" class="btn btn-success profile-button pass-button" >Simpan</button>
          @endif
        </div>
      </div>
    </form>
  </div>
</div>
    
@endsection