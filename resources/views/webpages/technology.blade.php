@extends('webpages.templates.master')

@section('title','Technology | Overnight Arbitrage')

@section('content')
        <div class="page-header">
          <h1>{{ trans('navbar.nav-tech') }}</h1>
        </div>

        @php
            $konten = true
        @endphp

        @if ($konten)
        @include('webpages.templates.comingsoon')
        @else
            
         {{-- Introducing Orrbit --}}
        <div class="container">
          <div class="intro-orrbit">
              <h2>introducing orrbit</h2>
              <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</h3>
          </div>

          {{-- Orrbit Image --}}
          <div class="intro-orrbit-img"></div>

          {{-- Orrbit Card --}}
          <div class="orrbit-card">
            <div class="card">
              <div class="card-body">
                <h2>latest expert advisor technology</h2>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</p>
              </div>
           </div>
            <div class="card">
              <div class="card-body">
                <h2>latest expert advisor technology</h2>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</p>
              </div>
           </div>
            <div class="card">
              <div class="card-body">
                <h2>latest expert advisor technology</h2>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</p>
              </div>
           </div>
            <div class="card">
              <div class="card-body">
                <h2>latest expert advisor technology</h2>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</p>
              </div>
           </div>
          </div>

          {{-- Integration with --}}
          <div class="integration">
            <h2>Integration with a lot of broker</h2>
          </div>

          <div class="orrbit-card">
            <div class="card">
              <div class="card-body">
                <h2>latest expert advisor technology</h2>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</p>
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <h2>latest expert advisor technology</h2>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</p>
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <h2>latest expert advisor technology</h2>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aenean massa lacus, cras viverra. Eget est magna laoreet viverra. Aliquam, neque sed dolor lobortis porta purus donec.</p>
              </div>
            </div>
          </div>

          {{-- Get start with orbit --}}
          <div class="orrbit-get-start">
            <h2>get started with orrbit</h2>
          </div>

          {{-- Orrbit Card --}}
          <div class="container">
          <div class="get-it-card">
            <div class="card">
              <div class="card-body">
                <div class="card-head">
                  <h3>Rp xx xxx</h3>
                  <a href="{{route('pricingPage')}}">
                    <button class="get-it-btn">Get it now</button>
                  </a>
                </div>
                <br />
                <hr />
                <div class="get-it-list">
                  <ul>
                    <li>The Swap Master Expert Advisors (Master and Slave Unit)</li>
                    <li>The Swap Master User Guide with many Best Practice Tips</li>
                    <li>Access to our Portfolio Research</li>
                    <li>Free Build Updates</li>
                    <li>Personalized 24/7 Support</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

          {{-- Orrbit Help --}}
          <div class="help-section row">
            <div class="help-item col-md-4">
              <div class="help-icon">
                <img src="{{url('/')}}/webpages/images/homepage/help.svg" alt="" />
              </div>
              <h3 class="help-title">Orrbit Help</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
                purus sit amet luctus venenatis
              </p>
            </div>
            <div class="help-item col-md-4">
              <div class="help-icon">
                <img src="{{url('/')}}/webpages/images/homepage/support.svg" alt="" />
              </div>
              <h3 class="help-title">Support</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
                purus sit amet luctus venenatis
              </p>
            </div>
            <div class="help-item col-md-4">
              <div class="help-icon">
                <img src="{{url('/')}}/webpages/images/homepage/discussion.svg" alt="" />
              </div>
              <h3 class="help-title">Discussion Blog and Articles</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
                purus sit amet luctus venenatis
              </p>
            </div>
          </div>
          {{-- End of Content --}}
        </div>

        @endif

@endsection