@extends('webpages.templates.master')

@section('content')
    <div class="container my-5">
        <h2 class="article-title">Swap Arbitrage as a trend!</h2>

        <div class="article-img mt-5">
            <img src="{{url('/')}}/webpages/images/blog/article-img.svg" class="img-fluid w-100" alt="">
        </div>

        <div class="article-author mt-3 mb-5">
            <div class="author-img"></div>
            <div class="author-name ml-3">
               <strong><p class="mb-0 mt-3">By Albert Dera</p></strong>
                <p>10 mins read</p>
            </div>
        </div>
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Convallis lacus, sit scelerisque et, aliquet aliquet. Ac arcu tincidunt aliquet platea id dolor leo. Odio nec potenti lacinia sem dui eu risus, tellus. </p>
        <p>Sit elit eros donec nibh quis aliquet laoreet in. Feugiat integer donec nulla maecenas pharetra sit volutpat. Enim, donec malesuada volutpat enim non pellentesque dictum nunc. Nisl in penatibus pellentesque mauris ullamcorper. Molestie ut sit commodo massa nec malesuada tortor, pulvinar enim. Nibh morbi lacus vel suspendisse sem sit mauris scelerisque. Eu aliquet lacinia viverra nulla morbi dictum. Habitant amet, gravida non, adipiscing ornare massa, est in. Cras sit blandit egestas nulla sollicitudin. Nibh mauris quam adipiscing faucibus phasellus est commodo. Proin enim in ipsum aenean faucibus sed mi risus vel.</p>
        <p>Read on: Lorem ipsum dolor sit amet</p>
        <p>Commodo, tempor ultrices praesent et habitant commodo dui facilisis. Enim sagittis eu enim felis arcu. Amet felis mauris viverra ornare aliquam pellentesque amet sit non. Bibendum ac at feugiat in vel. Venenatis magna placerat metus, interdum velit, ac sagittis leo. Mattis ut eu amet eget magna sodales leo sem velit. Senectus eu molestie tempor tincidunt eget. Porta enim habitasse viverra semper viverra sollicitudin sapien. Et at aliquam facilisis dis cras nunc arcu eu.</p>

        <h2 class="mt-5 mb-3" style="font-weight: 700">Lorem ipsum dolor sit amet.</h2>
        <p>Enim non in nascetur tempor turpis. Feugiat nulla scelerisque ipsum, vel. Nulla pharetra rhoncus egestas aliquam tortor dolor.
            Amet, lectus amet fermentum risus eget lacus vitae volutpat lorem. Sed sagittis, eget mauris auctor enim. Proin lobortis dui mattis condimentum in senectus faucibus. Vestibulum id quam in tortor, morbi dolor, proin gravida. Ligula a molestie mauris adipiscing est sapien nisl, ut in. Nunc facilisis fames molestie arcu.
        </p>
        <p>Consequat libero, cras amet ac lectus tempor, fames eleifend. Sed feugiat facilisis molestie et tempor tellus donec donec. Aliquet non magna amet viverra bibendum vitae tempus ipsum. Id a eget consequat vel ipsum. Est turpis sed vel vitae. Dolor nam adipiscing justo, orci, morbi. Et enim risus massa adipiscing ullamcorper faucibus. Vestibulum in quis ut mi sed venenatis, feugiat diam vitae. Viverra turpis malesuada cursus eu ullamcorper quam rhoncus. Sociis id phasellus eu in ut elit sed aliquet sem. Facilisis congue ultrices egestas arcu, tempor duis consequat. Pharetra, nulla ipsum diam, in id quis arcu. Non id et ultrices blandit orci, in. Blandit nibh aliquam imperdiet scelerisque diam. Quis felis blandit et augue adipiscing nisl habitasse.</p>
    
        <div class="article-card my-5">
            <div class="card">
                <div class="card-body">
                    <div class="article-card-img"></div>
                    <div class="article-card-author">
                        <h3>Finley Khouwira</h3>
                        <p style="font-size: 14px !important">Pharetra, nulla ipsum diam, in id quis arcu. Non id et ultrices blandit orci, in. Blandit nibh aliquam imperdiet scelerisque diam. Quis felis blandit et augue adipiscing nisl habitasse.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="learn-more">
            <h2>Learn more about Swap Arbitrage</h2>

            <div class="learn-more-card">
                
                    <div class="card">
                        <img class="card-img-top" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card1.svg" alt="Card image cap">
                        <div class="card-body">
                            <p>Arbitrage Recommendation from Orrbit</p>
                            <div class="card-bottom">
                                <img src="{{url('/')}}/webpages/images/swap-arbitrage/johanna.svg" alt="">
                                <div class="card-name ml-3 mt-2">
                                    <p class="mb-0"><strong>By Johanna</strong></p>
                                    <p>10 mins read</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                    <div class="card">
                        <img class="card-img-top" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card2.svg" alt="Card image cap">
                        <div class="card-body">
                            <p>Federal Reserve Orders New Round of Stress Tests</p>
                            <div class="card-bottom">
                                <img src="{{url('/')}}/webpages/images/swap-arbitrage/ariella.svg" alt="">
                                <div class="card-name ml-3 mt-2">
                                    <p class="mb-0"><strong>By Johanna</strong></p>
                                    <p>10 mins read</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                    <div class="card">
                        <img class="card-img-top" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card3.svg" alt="Card image cap">
                        <div class="card-body">
                            <p>Currency Pair Definition</p>
                            <div class="card-bottom">
                                <img src="{{url('/')}}/webpages/images/swap-arbitrage/riel.svg" alt="">
                                <div class="card-name ml-3 mt-2">
                                    <p class="mb-0"><strong>By Johanna</strong></p>
                                    <p>10 mins read</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
    
    
    
    
    </div>
@endsection