@extends('webpages.templates.master')

@section('title','Pricing | Overnight Arbitrage')

@section('content')
    <div class="page-header">
      <h1>{{ trans('navbar.nav-price') }}</h1>
    </div>

    @php
        $konten = true
    @endphp

    @if ($konten)
    <div class="container">
      <section class="pricing py-5">
        <div class="container">
          <div class="row">

            {{-- Plan 1 --}}
            <div class="col-lg-4">
              <div class="card mb-5 mb-lg-0">
                <div class="card-body">
                  <h3 class="card-title text-center">1 month</h3>
                  <h2 class="card-price text-center">
                  @if ($locale == 'cn')
                    {{'¥' . round($robots->robotPrice3 * $currency->CNY)}}
                  @elseif($locale == 'id')
                    <?php $harga = round($robots->robotPrice3 * $currency->IDR) ?>
                    {{'Rp ' . number_format($harga,0,',','.')}}
                  @else
                    {{'$'.$robots->robotPrice3}}
                  @endif    
                  </h2>
                  <a href="{{ url('add-to-cart/'.$robots->id.'/1')}}" class="btn btn-block text-uppercase">buy now</a>
                  <hr>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                  </ul>
                </div>
              </div>
            </div>

            {{-- Plan 2 --}}
            <div class="col-lg-4">
              <div class="card mb-5 mb-lg-0">
                <div class="card-body">
                  <h3 class="card-title text-center">3 months</h3>
                  <h2 class="card-price text-center">
                  @if ($locale == 'cn')
                    {{'¥' . round($robots->robotPrice2 * $currency->CNY)}}
                  @elseif($locale == 'id')
                    <?php $harga = round($robots->robotPrice2 * $currency->IDR) ?>
                    {{'Rp ' . number_format($harga,0,',','.')}}
                  @else
                    {{'$'.$robots->robotPrice2}}
                  @endif     
                  </h2>
                  <a href="{{ url('add-to-cart/'.$robots->id.'/3')}}" class="btn btn-block text-uppercase">buy now</a>
                  <hr>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                  </ul>
                </div>
              </div>
            </div>

            {{-- Plan 3 --}}
            <div class="col-lg-4 plan3">
              <div class="card mb-5 mb-lg-0">
                <div class="card-header">BEST DEAL!</div>
                <div class="card-body">
                  <h3 class="card-title text-center">1 year</h3>
                  <h2 class="card-price text-center">
                  @if ($locale == 'cn')
                    {{'¥' . round($robots->robotPrice1 * $currency->CNY)}}
                  @elseif($locale == 'id')
                    <?php $harga = round($robots->robotPrice1 * $currency->IDR) ?>
                    {{'Rp ' . number_format($harga,0,',','.')}}
                  @else
                    {{'$'.$robots->robotPrice1}}
                  @endif   
                  </h2>
                  <a href="{{ url('add-to-cart/'.$robots->id.'/12')}}" class="btn btn-block text-uppercase">buy now</a>
                  <hr>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    {{-- @include('webpages.templates.comingsoon')         --}}
    @else


     {{-- Plans & Pricing --}}
    <div class="container">
      <section class="pricing py-5">
        <div class="container">
          <div class="row">

            {{-- Plan 1 --}}
            <div class="col-lg-4">
              <div class="card mb-5 mb-lg-0">
                <div class="card-body">
                  <h3 class="card-title text-center">1 month</h3>
                  <h2 class="card-price text-center">Rp xx xxx</h2>
                  <a href="#" class="btn btn-block text-uppercase">buy now</a>
                  <hr>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                  </ul>
                </div>
              </div>
            </div>

            {{-- Plan 2 --}}
            <div class="col-lg-4">
              <div class="card mb-5 mb-lg-0">
                <div class="card-body">
                  <h3 class="card-title text-center">3 months</h3>
                  <h2 class="card-price text-center">Rp xx xxx</h2>
                  <a href="#" class="btn btn-block text-uppercase">buy now</a>
                  <hr>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                  </ul>
                </div>
              </div>
            </div>

            {{-- Plan 3 --}}
            <div class="col-lg-4 plan3">
              <div class="card mb-5 mb-lg-0">
                <div class="card-header">BEST DEAL!</div>
                <div class="card-body">
                  <h3 class="card-title text-center">1 year</h3>
                  <h2 class="card-price text-center">Rp xx xxx</h2>
                  <a href="#" class="btn btn-block text-uppercase">buy now</a>
                  <hr>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                    <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>


    <!-- Features -->
    <div class="how-it-work">
      <div class="container">
        <h2>Features</h2>
          <div class="step1 row" >
            <div class="col-md-8" >
              <div class="how-img"></div>
            </div>
              <div class="step col-md-4" >
                <p>Orrbit Expert Advisor</p>
                <ul>
                  <li>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
                    delectus commodi magni, nemo consectetur laborum.
                  </li>
                  <br />
                  <li>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
                    delectus commodi magni, nemo consectetur laborum.
                  </li>
                </ul>
              </div>
          </div>
          <div class="step2 row" >
            <div class="col-md-8 step2-img" >
              <div class="how-img"></div>
            </div>
              <div class="step col-md-4 step2-text" >
                <p>Orrbit User Guide</p>
                <ul>
                  <li>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
                    delectus commodi magni, nemo consectetur laborum.
                  </li>
                  <br />
                  <li>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
                    delectus commodi magni, nemo consectetur laborum.
                  </li>
                </ul>
              </div>
          </div>
          <div class="step1 row" >
            <div class="col-md-8" >
              <div class="how-img"></div>
            </div>
              <div class="step col-md-4" >
                <p>Research & Record</p>
                <ul>
                  <li>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
                    delectus commodi magni, nemo consectetur laborum.
                  </li>
                  <br />
                  <li>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
                    delectus commodi magni, nemo consectetur laborum.
                  </li>
                </ul>
              </div>
          </div>
      </div>
    </div>
    <!-- End of Features -->

  <div class="container">
        <div class="faq">
          <h2>Frequently Asked Questions</h2>
          
          <div class="accordion" id="accordionExample">
            <div class="card" style="border: transparent">
              <div id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <div class="question">
                  Lorem ipsum dolor sit amet, consectetur adipiscing
                  <button>                
                    <img src="{{url('/')}}/webpages/images/pricing/vector.svg" alt="">
                  </button>
              </div>
              </div>
          
              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
        
            <div class="card" style="border: transparent">
              <div id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <div class="question">
                  Lorem ipsum dolor sit amet, consectetur adipiscing
                  <button>                
                    <img src="{{url('/')}}/webpages/images/pricing/vector.svg" alt="">
                  </button>
                </div>
              </div>
          
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
        
            <div class="card" style="border: transparent">
              <div id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                <div class="question">
                  Lorem ipsum dolor sit amet, consectetur adipiscing
                  <button>                
                    <img src="{{url('/')}}/webpages/images/pricing/vector.svg" alt="">
                  </button>
                </div>
              </div>
          
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
        
            <div class="card" style="border: transparent">
              <div id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                <div class="question">
                  Lorem ipsum dolor sit amet, consectetur adipiscing
                  <button>                
                    <img src="{{url('/')}}/webpages/images/pricing/vector.svg" alt="">
                  </button>
                </div>
              </div>
          
              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
        
            <div class="card" style="border: transparent">
              <div id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                <div class="question">
                  Lorem ipsum dolor sit amet, consectetur adipiscing
                  <button>                
                    <img src="{{url('/')}}/webpages/images/pricing/vector.svg" alt="">
                  </button>
                </div>
              </div>
          
              <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
          </div>
        
        </div>
  </div>


    @endif




   

@endsection