@extends('webpages.templates.master')

@section('title','Blog | Overnight Arbitrage')

@section('content')
<div class="page-header">
    <h1>{{ trans('navbar.nav-blog') }}</h1>
</div>

@php
$konten = false;
@endphp

@if ($konten)

@include('webpages.templates.comingsoon')

@else
    
<div class="container">
    <div class="blog-menu">
        <ul>
            <li><a href="#">Weekly Highlight</a></li>
            <li><a href="#">All</a></li>
            <li><a href="#">Case Studies</a></li>
            <li><a href="#">Innovation</a></li>
            <li><a href="#">Version Update</a></li>
            <li><a href="#">Register</a></li>
            <li>
                <a href="#">
                    <img src="{{url('/')}}/webpages/images/blog/Union.svg" alt="">
                </a>
            </li>
        </ul>
    </div>

    <div class="blog-trend">
        <div class="row">
            <div class="col-md-6 blog-img">
                <img src="{{url('/')}}/webpages/images/blog/money.svg" class="img-fluid" alt="">
            </div>
            <div class="col-md-6">
                <div class="blog-desc">
                    <h2>Swap Arbitrage as a trend!</h2>
                    <div class="blog-author">
                        <img src="{{url('/')}}/webpages/images/swap-arbitrage/johanna.svg" class="mr-3" alt="">
                        <div class="card-name ml-3 mt-2">
                            <p class="mb-0"><strong>By Johanna</strong></p>
                            <p>10 mins read</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="blog-card">
            <a href="{{route('articlePage')}}" class="custom-blog-card">
                <div class="card">
                    <img class="card-img-top img-fluid" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card1.svg" alt="Card image cap">
                    <div class="card-body">
                        <p>Arbitrage Recommendation from Orrbit</p>
                        <div class="card-bottom">
                            <img src="{{url('/')}}/webpages/images/swap-arbitrage/johanna.svg" alt="">
                            <div class="card-name ml-3 mt-2">
                                <p class="mb-0"><strong>By Johanna</strong></p>
                                <p>10 mins read</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="{{route('articlePage')}}" class="custom-blog-card">
                <div class="card">
                    <img class="card-img-top img-fluid" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card2.svg" alt="Card image cap">
                    <div class="card-body">
                        <p>Federal Reserve Orders New Round of Stress Tests</p>
                        <div class="card-bottom">
                            <img src="{{url('/')}}/webpages/images/swap-arbitrage/ariella.svg" alt="">
                            <div class="card-name ml-3 mt-2">
                                <p class="mb-0"><strong>By Johanna</strong></p>
                                <p>10 mins read</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="{{route('articlePage')}}" class="custom-blog-card">
                <div class="card">
                    <img class="card-img-top img-fluid" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card3.svg" alt="Card image cap">
                    <div class="card-body">
                        <p>Currency Pair Definition</p>
                        <div class="card-bottom">
                            <img src="{{url('/')}}/webpages/images/swap-arbitrage/riel.svg" alt="">
                            <div class="card-name ml-3 mt-2">
                                <p class="mb-0"><strong>By Johanna</strong></p>
                                <p>10 mins read</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

            <a href="{{route('articlePage')}}" class="custom-blog-card">
                <div class="card">
                    <img class="card-img-top img-fluid" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card1.svg" alt="Card image cap">
                    <div class="card-body">
                        <p>Arbitrage Recommendation from Orrbit</p>
                        <div class="card-bottom">
                            <img src="{{url('/')}}/webpages/images/swap-arbitrage/johanna.svg" alt="">
                            <div class="card-name ml-3 mt-2">
                                <p class="mb-0"><strong>By Johanna</strong></p>
                                <p>10 mins read</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="{{route('articlePage')}}" class="custom-blog-card">
                <div class="card">
                    <img class="card-img-top img-fluid" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card2.svg" alt="Card image cap">
                    <div class="card-body">
                        <p>Federal Reserve Orders New Round of Stress Tests</p>
                        <div class="card-bottom">
                            <img src="{{url('/')}}/webpages/images/swap-arbitrage/ariella.svg" alt="">
                            <div class="card-name ml-3 mt-2">
                                <p class="mb-0"><strong>By Johanna</strong></p>
                                <p>10 mins read</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="{{route('articlePage')}}" class="custom-blog-card">
                <div class="card">
                    <img class="card-img-top img-fluid" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card3.svg" alt="Card image cap">
                    <div class="card-body">
                        <p>Currency Pair Definition</p>
                        <div class="card-bottom">
                            <img src="{{url('/')}}/webpages/images/swap-arbitrage/riel.svg" alt="">
                            <div class="card-name ml-3 mt-2">
                                <p class="mb-0"><strong>By Johanna</strong></p>
                                <p>10 mins read</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
        </div>

    <div class="blog-pagination">
        <ul>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">Next</a></li>
        </ul>
    </div>
</div>


@endif



@endsection