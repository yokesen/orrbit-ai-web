{{-- Modal Checkout  --}}


<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
        <div class="modal-header" style="border-bottom: transparent">

            <div class="row" style="row-gap: 10px">
                <div class="col-md-3">Dari: Admin, Inc.</div>
                <div class="col-md-4"> Kepada: {{CRUDBooster::myName()}} </div>
                @foreach (Cart::content()->sortByDesc('id') as $n => $cart)
                <div class="col-md-5">Tanggal Pembelian: {{date('d M Y', strtotime($cart->name->created_at))}}</div>
                @break
                @endforeach
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {{-- Table --}}

            <table class="table table-borderless table-hover">
                <thead>
                    <tr>
                        <th scope="col">Produk</th>
                        <th scope="col">Periode</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Harga</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (Cart::content()->sortByDesc('id') as $n => $check)
                    <?php
                    $service = "";
                    if ($check->options['service']['install'] != 0)
                        $service .= "install ";
                    if ($check->options['service']['maintenance'] != 0)
                        $service .= "maintenance ";
                    if ($check->options['service']['vps'] != 0)
                        $service .= "vps";

                    ?>
                    <tr>
                        <td>{{$check->name->robotLongName}}</td>
                        <td>{{$check->options->periode['sebutan']}}</td>
                        <td></td>
                        <td>${{$check->options->periode['harga']}}</td>
                    </tr>

                    <?php
					$biayaService = 0;
					?>
					@foreach ($jasa as $service)
					@if ($check->options->service[$service->jasaName] != 0)
                    <tr>
                        <td></td>
                        <td></td>
                        <td>{{$service->jasaName}}</td>
                        <td>+${{$check->options->service[$service->jasaName]}}</td>
                    </tr>
                    <?php
					$biayaService += $check->options->service[$service->jasaName];
					?>
                    @endif
					@endforeach
					<?php
					$totalService += $biayaService;
					$totalRobot += $check->price;

					if ($check->options->promo['amount'] > 0) {
						$potongan = $check->options->promo['amount'];
						$kodenya = $check->options->promo['voucher'];
					}
                    ?>
                    @endforeach
                    <?php
                    $subtotal = $totalRobot + $totalService;
                    $diskon = $potongan * $totalRobot * 0.01;
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Subtotal</td>
                        <td>${{$subtotal}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Discount</td>
                        <td>-${{$diskon}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>${{$subtotal-$diskon}}</b></td>
                    </tr>
                </tbody>
            </table>

            {{-- -------- --}}
        </div>
        <div class="modal-footer" style="border-top: transparent">
            <button type="button" class="btn beli-lagi">Checkout</button>
        </div>
    </div>
</div>