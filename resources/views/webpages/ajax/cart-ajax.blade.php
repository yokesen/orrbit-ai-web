<div class="col-md-7">
    @foreach (Cart::content()->sortBy('id') as $n => $cart)
    <?php
    // dd($cart);
    ?>
    <div class="cart-block mt-3">
        <div class="mt-3">
            <div class="form-group">
                <h5>{{$cart->name->robotLongName}}</h5>
                <a href="{{route('removeFromCart',$cart->rowId)}}"><i class="fas fa-times float-right"></i></a>
            </div>
            <h6><b>Pilih Durasi</b></h6>
            <div class="row text-center pt-3">
                <div class="col-md-4">
                    <label>
                        <input type="radio" name="radio_robot" id="length1" class="card-radio-robot d-none redio_robot" value="1" onchange="rentPeriode('1','{{$n}}')" {{$cart->options->periode['kali'] == 1 ? 'checked' : ''}}>
                        <div class="card {{$cart->options->periode['kali'] == 1 ? 'color-sewarobot' : ''}}">
                            <p><b>1 bulan</b></p>
                            <b>
                                @if ($locale == 'cn')
                                    {{'¥' . round($cart->name->robotPrice3 * $currency->CNY) . '/month'}}
                                @elseif($locale == 'id')
                                    <?php $harga = round($cart->name->robotPrice3 * $currency->IDR) ?>
                                    {{'Rp ' . number_format($harga,0,',','.') . '/bulan'}}
                                @else
                                    {{'$'.$cart->name->robotPrice3 . '/month'}}
                                @endif
                            </b>
                        </div>
                    </label>
                </div>
                <div class="col-md-4">
                    <label>
                        <input type="radio" name="radio_robot" id="length2" class="card-radio-robot d-none redio_robot" value="3" onchange="rentPeriode('3','{{$n}}')" {{$cart->options->periode['kali'] == 3 ? 'checked' : ''}}>
                        <div class="card {{$cart->options->periode['kali'] == 3 ? 'color-sewarobot' : ''}}">
                            <p><b>3 bulan</b></p>
                            <p class="price-opt-through">
                                @if ($locale == 'cn')
                                    {{'¥' . round($cart->name->robotPrice3 * $currency->CNY) . '/month'}}
                                @elseif($locale == 'id')
                                    <?php $harga = round($cart->name->robotPrice3 * $currency->IDR) ?>
                                    {{'Rp ' . number_format($harga,0,',','.') . '/bulan'}}
                                @else
                                    {{'$'.$cart->name->robotPrice3 . '/month'}}
                                @endif
                            </p>
                            <b>
                                @if ($locale == 'cn')
                                    {{'¥' . round(($cart->name->robotPrice2 * $currency->CNY) / 3 , 2) . '/month'}}
                                @elseif($locale == 'id')
                                    <?php $harga = round(($cart->name->robotPrice2 * $currency->IDR) / 3 , 2 ) ?>
                                    {{'Rp ' . number_format($harga,0,',','.') . '/bulan'}}
                                @else
                                    {{'$'. round($cart->name->robotPrice2 / 3 , 2) . '/month'}}
                                @endif    
                            </b>
                            <p class="price-opt-total">
                                <b>
                                    @if ($locale == 'cn')
                                        {{'¥' . round($cart->name->robotPrice2 * $currency->CNY)}}
                                    @elseif($locale == 'id')
                                        <?php $harga = round($cart->name->robotPrice2 * $currency->IDR) ?>
                                        {{'Rp ' . number_format($harga,0,',','.')}}
                                    @else
                                        {{'$'.$cart->name->robotPrice2}}
                                    @endif
                                </b>
                            </p>
                        </div>
                    </label>
                </div>
                <div class="col-md-4">
                    <label>
                        <input type="radio" name="radio_robot" id="length3" class="card-radio-robot d-none redio_robot" value="12" onchange="rentPeriode('12','{{$n}}')" {{$cart->options->periode['kali'] == 12 ? 'checked' : ''}}>
                        <div class="card {{$cart->options->periode['kali'] == 12 ? 'color-sewarobot' : ''}}">
                            <p><b>1 tahun</b></p>
                            <p class="price-opt-through">
                                @if ($locale == 'cn')
                                    {{'¥' . round($cart->name->robotPrice3 * $currency->CNY) . '/month'}}
                                @elseif($locale == 'id')
                                    <?php $harga = round($cart->name->robotPrice3 * $currency->IDR) ?>
                                    {{'Rp ' . number_format($harga,0,',','.') . '/bulan'}}
                                @else
                                    {{'$'.$cart->name->robotPrice3 . '/month'}}
                                @endif
                            </p>
                            <b>
                                @if ($locale == 'cn')
                                    {{'¥' . round(($cart->name->robotPrice1 * $currency->CNY) / 12 , 2) . '/month'}}
                                @elseif($locale == 'id')
                                    <?php $harga = round(($cart->name->robotPrice1 * $currency->IDR) / 12 , 2 ) ?>
                                    {{'Rp ' . number_format($harga,0,',','.') . '/bulan'}}
                                @else
                                    {{'$'. round($cart->name->robotPrice1 / 12 , 2) . '/month'}}
                                @endif
                            </b>
                            <p class="price-opt-total">
                                <b>
                                    @if ($locale == 'cn')
                                        {{'¥' . round($cart->name->robotPrice1 * $currency->CNY)}}
                                    @elseif($locale == 'id')
                                        <?php $harga = round($cart->name->robotPrice1 * $currency->IDR) ?>
                                        {{'Rp ' . number_format($harga,0,',','.')}}
                                    @else
                                        {{'$'.$cart->name->robotPrice1}}
                                    @endif	
                                </b>
                            </p>
                        </div>
                    </label>
                </div>
            </div>

            @foreach ($jasa as $service)
            <div class="cart-block mt-3 p-3">
                @if ($cart->options->service[$service->jasaName] != 0)
                <input type="checkbox" id="{{$service->jasaName}}" name="additional" value="{{$service->jasaName}}" onchange="removeOption('{{$service->jasaName}}','{{$n}}')" checked>
                @else
                <input type="checkbox" id="{{$service->jasaName}}" name="additional" value="{{$service->jasaName}}" onchange="addOption('{{$service->jasaName}}','{{$n}}')">
                @endif
                <label class="font-weight-bold">{{$service->jasaLongName}}</label>
                <p class="float-right">+ 
                    @if ($locale == 'cn')
                        {{'¥' . round($service->jasaPrice * $currency->CNY)}}
                    @elseif($locale == 'id')
                        <?php $harga = round($service->jasaPrice * $currency->IDR) ?>
                        {{'Rp ' . number_format($harga,0,',','.')}}
                    @else
                        {{'$'.$service->jasaPrice}}
                    @endif
                    /{{$service->jasaPeriode}} </p>
            </div>
            @endforeach
        </div>
    </div>
    @endforeach
</div>

<div class="col-md-5">
    <div class="cart-block mt-3">
        <h5><b>Detail Orders</b></h5>
        @foreach (Cart::content()->sortBy('id') as $n => $check)
        <div class="pt-3">
            <div class="pricing-text">
                <h6 style="margin: 0;">{{$check->name->robotLongName}}</h6>

                <p style="font-size: 12px;">Robot Trading</p>
            </div>
            <div class="pricing-text">

                <p id="length">{{$check->options->periode['sebutan']}}</p>
                <p class="float-right"><b id="harga">
                    @if ($locale == 'cn')
                        {{'¥' . round($check->price * $currency->CNY)}}
                    @elseif($locale == 'id')
                        <?php $harga = round($check->price * $currency->IDR) ?>
                        {{'Rp ' . number_format($harga,0,',','.')}}
                    @else
                        {{'$'.$check->price}}
                    @endif    
                </b></p>
            </div>
            <?php
            $biayaService = 0;
            ?>
            @foreach ($jasa as $service)
            @if ($check->options->service[$service->jasaName] != 0)
            <div class="pricing-text">
                <p>{{$service->jasaName}}</p>
                <p class="float-right"><b>
                    @if ($locale == 'cn')
                        {{'¥' . round($check->options->service[$service->jasaName] * $currency->CNY)}}
                    @elseif($locale == 'id')
                        <?php $harga = round($check->options->service[$service->jasaName] * $currency->IDR) ?>
                        {{'Rp ' . number_format($harga,0,',','.')}}
                    @else
                        {{'$'.$check->options->service[$service->jasaName]}}
                    @endif	    
                </b></p>
            </div>
            <?php
            $biayaService += $check->options->service[$service->jasaName];
            ?>
            @endif
            @endforeach
            <?php
            $totalService += $biayaService;
            $totalRobot += $check->price;

            if ($check->options->promo['amount'] > 0) {
                $potongan = $check->options->promo['amount'];
                $kodenya = $check->options->promo['voucher'];
            }
            ?>
        </div>
        @endforeach

        <?php
        $subtotal = $totalRobot + $totalService;
        $diskon = $potongan * $totalRobot * 0.01;
        ?>

        @if ($potongan > 0)
        <div class="input-group pt-3">
            <input type="text" name="voucher" class="form-control" placeholder="{{$kodenya}}" disabled>
            <div class="input-group-append">
                <button class="btn btn-submit-checkout btn-sm" disabled>Submit</button>
            </div>
        </div>

        <a href="javascript:void(0)" class="float-right" onclick="removeVoucher()"> <small>remove voucher</small> </a>

        @else
        <div class="input-group pt-3">
            <input type="text" name="voucher" id="kodeVoucher" class="form-control" placeholder="Masukkan kode voucher">
            <div class="input-group-append">
                <button class="btn btn-submit-checkout btn-sm" onclick="kodeVoucher()">Submit</button>
            </div>
        </div>

        @endif

        <div class="pt-5">
            <div class="pricing-text">
                <p>Sub-total</p>
                <p class="float-right"><b>
                    @if ($locale == 'cn')
                        {{'¥' . round($subtotal * $currency->CNY)}}
                    @elseif($locale == 'id')
                        <?php $harga = round($subtotal * $currency->IDR) ?>
                        {{'Rp ' . number_format($harga,0,',','.')}}
                    @else
                        {{'$'.$subtotal}}
                    @endif    
                </b></p>
            </div>
            <div class="pricing-text">
                <p>Voucher</p>
                <p class="float-right"><b>-
                    @if ($locale == 'cn')
                        {{'¥' . round($diskon * $currency->CNY)}}
                    @elseif($locale == 'id')
                        <?php $harga = round($diskon * $currency->IDR) ?>
                        {{'Rp ' . number_format($harga,0,',','.')}}
                    @else
                        {{'$'.$diskon}}
                    @endif	    
                </b></p>
            </div>
            <div class="pricing-text">
                <p><b>Total</b></p>
                <p class="float-right"><b>
                    @if ($locale == 'cn')
                        {{'¥' . round(($subtotal-$diskon) * $currency->CNY)}}
                    @elseif($locale == 'id')
                        <?php $harga = round(($subtotal-$diskon) * $currency->IDR) ?>
                        {{'Rp ' . number_format($harga,0,',','.')}}
                    @else
                        {{'$'.($subtotal-$diskon)}}
                    @endif	    
                </b></p>
            </div>
        </div>
        <div class="pricing-btn_wrap text-center pt-5">
            @if (CRUDBooster::myID())
            {{-- <a href="{{route('processCheckout')}}" class="btn btn-checkout" style="width: 100%;">CHECKOUT</a> --}}
            <a onclick="getModalData()" class="btn btn-checkout" style="width: 100%;">CHECKOUT</a>
            @else
            <a href="#loginModal" data-toggle="modal" tabindex="-1" aria-disabled="true" class="btn btn-checkout" style="width: 100%;">CHECKOUT</a>
            @endif
        </div>
    </div>
</div>