@extends('webpages.templates.master')
@section('title', 'setting')
@section('content')
<div class="container-fluid container-setting">
  <div class="container-setting-inside">
    <div class="row">
      <div class="col-12 col-md-3 card-wrapper">
        <div class="card setting-card-menu" >
          <div class="card-body">
            <div class="setting-menu-header">
              @php
                  $data = getUserData($user);

                  // dd($data);
              @endphp
              <img class="setting-profile-photo" src="<?php echo ($data[0]->providerOrigin == 'facebook' || $data[0]->providerOrigin == 'google')?  $data[0]->photo :'/uploads/user_photo/user_default.png'; ?>" alt="User Image">
              <h5 class="card-title setting-profile-name">{{$data[0]->name}} {{$data[0]->lastName}}</h5>
            </div>
            <div class="setting-menu">
              <a href="{{route('profile')}}" class="card-link setting-card-link {{set_active('profile')}}">Profile</a>
              <a href="{{route('changePassword')}}" class="card-link setting-card-link {{set_active('changePassword')}}">Password</a>
              <a href="#" class="card-link setting-card-link">Pembayaran</a>
              <a href="{{route('privacyPolicy')}}" class="card-link setting-card-link {{set_active('privacyPolicy')}}">Privacy Policy</a>
              <a href="{{route('complain')}}" class="card-link setting-card-link {{set_active('complain')}}">Laporan</a>
              <a href="{{route('help')}}" class="card-link setting-card-link {{set_active('help')}}">Bantuan</a>
            </div>
          </div>
        </div>
        <div class="form-group setting-menu-mobile">
          <select class="form-control setting-mobile" onchange="window.location.href=this.options[this.selectedIndex].value">
            {{-- <option class="mobile-link-option" disabled>Pilih Menu</option> --}}
            <option class="mobile-link-option" value="{{route('profile')}}" {{set_active('profile')}}>Profile</option>
            <option class="mobile-link-option" value="{{route('changePassword')}}" {{set_active('changePassword')}}>Password</option>
            <option class="mobile-link-option" value="#">Pembayaran</option>
            <option class="mobile-link-option" value="{{route('privacyPolicy')}}"  {{set_active('privacyPolicy')}}>Privacy Policy</option>
            <option class="mobile-link-option" value="{{route('complain')}}"  {{set_active('complain')}}>Laporan</option>
            <option class="mobile-link-option" value="{{route('help')}}"  {{set_active('help')}}>Bantuan</option>
          </select>
        </div>
      </div>
      @yield('setting')
    </div>
  </div>
</div>
    
@endsection