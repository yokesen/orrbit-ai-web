
@extends('crudbooster::admin_template')

@section('content')
  <!-- Your Page Content Here -->
  <div class="pad margin no-print">
    <div class="callout callout-info" style="margin-bottom: 0!important;">
      <h4><i class="fa fa-info"></i> Note:</h4>
      This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
    </div>
  </div>

  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        {{-- <h2 class="page-header">
          <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">

        </h2> --}}
      </div>
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Dari
        
        <address>
          <strong>Sewa Robot</strong><br>
          Address <br>
          City<br>
          Phone: 021-50029292<br>
          Email: info@SewaRobot.com
          <br>
          <br>
          {{-- <label for="toko">Cabang/Depo</label> --}}
          {{-- <br> --}}
          <p></p>
        </address>

      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Kepada
        <address>
          <p><strong>{{$salesData->name}} {{$salesData->lastName}}</strong> </p>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Detail Transaksi
          <p><b>Invoice : {{$salesData->invoice}} </b></p>
          <p>Order Date : {{date('d M Y', strtotime($salesData->salesTime))}}</p>
          <br>
          <br>
          <h3 class="text-danger">keranjang</h3>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    
    <div class="row justify-content-center">
      <div class="col-xs-10 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Robot</th>
              <th>Qty</th>
              <th>Periode</th>
              <th>Deskripsi</th>
              <th style="width: 200px !important; text-align: right;">Subtotal</th>
            </tr>
          </thead>
          <tbody>
            
            <?php $productDetails = unserialize($salesData->salesSart); ?>
            
            @foreach ($productDetails as $productDetail )
            <?php 
              $services = "";
              if($productDetail->options['service']['install'] != 0)
                $services.="install";
              if($productDetail->options['service']['maintenance'] != 0)
                if($services == 'install')
                  $services.=", maintenance";
                else
                  $services.="maintenance";
              if($productDetail->options['service']['vps'] != 0)
              if($services != '')
                  $services.=", vps";
                else
                  $services.="vps";
            ?>
              <tr>
                <td>{{$productDetail->name->robotLongName}}</td>
                <td>{{$productDetail->qty}}</td>       
                <td>{{$productDetail->options['periode']['sebutan']}}</td>       
                <td>{{$services}}</td>       
                <td style="text-align: right">Rp. {{number_format($productDetail->options['periode']['harga'] + $productDetail->options['service']['install'] + $productDetail->options['service']['maintenance'] + $productDetail->options['service']['vps'],'0','.','.')}}</td>       
              </tr>
              @endforeach
              <tr>
                <td colspan="4" style="text-align: right"><b>Total</b></td>
                <td style="text-align: right">Rp. {{number_format($salesData->salesSubtotal,'0','.','.')}}</td>
              </tr>
              @if ($salesData->salesDiscount != 0)
              <tr>
                <td colspan="4" style="text-align: right"><b>Discount</b></td>
                <td style="text-align: right">Rp. {{number_format($salesData->salesDiscount,'0','.','.')}}</td>     
              </tr>
              <tr>
                <td colspan="4" style="text-align: right"><b>Total after discount</b></td>
                <td style="text-align: right">Rp. {{number_format($salesData->salesTotal,'0','.','.')}}</td>     
              </tr>
              @endif
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <span onclick="window.print();" class="btn btn-default"><i class="fa fa-print"></i> Print</span>

        

        
      </div>
    </div>
  </section>
  <div id="editor"></div>

  <!-- /.content -->
  <div class="clearfix"></div>

  <!-- /.content-wrapper -->
@endsection