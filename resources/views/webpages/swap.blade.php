@extends('webpages.templates.master')
@section('title','Swap Arbitrage | Overnight Arbitrage')

@section('content')
<div class="page-header">
    <h1>{{ trans('swap-arbitrage.swap-arbitrage-title') }}</h1>
</div>

{{-- What is Swap Arbitrage --}}
<div class="container">
    <div class="swap-arbitrage">
        <h2>{{ trans('swap-arbitrage.swap-arbitrage-question') }}</h2>
        <h3>{{ trans('swap-arbitrage.swap-arbitrage-caption') }}</h3>
    </div>
</div>


{{-- Swap Arbitrage Definition --}}
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <img src="{{url('/')}}/webpages/images/swap-arbitrage/swap.svg" class="img-fluid" alt="">
            </div>
            <div class="col-md-4 swap-desc">
                <h3>{{ trans('swap-arbitrage.swap-desc-title') }}</h3>
                <p>{{ trans('swap-arbitrage.swap-desc-caption') }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 arbitrage-img">
                <img src="{{url('/')}}/webpages/images/swap-arbitrage/arbitrage.svg" class="img-fluid" alt="">
            </div>
            <div class="col-md-4 swap-desc arbitrage-desc">
                <h3>{{ trans('swap-arbitrage.arbitrage-desc-title') }}</h3>
                <p>{{ trans('swap-arbitrage.arbitrage-desc-caption') }}</p>
            </div>
            
        </div>
    </div>

    {{-- <div class="container">
        <h3 class="lorem">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Justo volutpat eget senectus nisl vel in maecenas facilisi. At hac aliquet donec lobortis egestas. Dictum diam integer fermentum consectetur. Diam fringilla maecenas sollicitudin duis. Odio fermentum nullam egestas cursus sit. Commodo id quam mattis dignissim eu, eget.
        </h3>
    </div> --}}

   
    {{-- How Swap Arbitrage --}}
    <div class="how-swap mt-5">
        <div class="container">
          <h2>{{ trans('why-swap.why-swap-title') }}</h2>
            <div class="step1 row" >
              <div class="col-md-6" >
                <div class="how-swap-img">
                    <img src="{{url('/')}}/webpages/images/swap-arbitrage/riskless.png" class="img-fluid" alt="">
                </div>
              </div>
                <div class="step col-md-6" >
                  <h3>{{ trans('why-swap.first-title') }}</h3>
                  <p>{{ trans('why-swap.first-caption') }}</p>
                </div>
            </div>
            <div class="step2 row">
                <div class="step2-img col-md-6" >
                    <div class="how-swap-img">
                        <img src="{{url('/')}}/webpages/images/swap-arbitrage/autopilot.png" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="step col-md-6" >
                    <h3>{{ trans('why-swap.second-title') }}</h3>
                    <p>{{ trans('why-swap.second-caption') }}</p>
                </div>
            </div>
            <div class="step1 row" >
              <div class="col-md-6" >
                <div class="how-swap-img">
                    <img src="{{url('/')}}/webpages/images/swap-arbitrage/gain.png" class="img-fluid" alt="">
                </div>
              </div>
                <div class="step col-md-6" >
                    <h3>{{ trans('why-swap.third-title') }}</h3>
                    <p>{{ trans('why-swap.third-caption') }}</p>
                </div>
            </div>
        </div>
      </div>


    {{-- Orrbit can help you --}}
      <div class="container">
          <div class="orrbit-help">
          {{-- <div class="row"> --}}
              {{-- <div class="col-md-4">
                <div class="orrbit-help-desc">
                    <h3>ORRBIT CAN HELP YOU WITH SWAP ARBITRAGE</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Scelerisque elit morbi tortor, accumsan, arcu, nibh enim amet pretium. Ipsum dui fermentum senectus diam ante at arcu platea viverra. Tincidunt quis dolor pulvinar tristique amet, varius id condimentum nulla.</p>
                </div>
              </div> --}}
              <div>
                <div class="swap-get-it-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-head">
                          <h2>$99.9</h2>
                          <a href="{{route('pricingPage')}}">
                            <button class="get-it-btn">Get it now</button>
                        </a>
                        </div>
                        <br />
                        <hr />
                        <div class="get-it-list">
                          <ul>
                            <li>The Swap Master Expert Advisors (Master and Slave Unit)</li>
                            <li>The Swap Master User Guide with many Best Practice Tips</li>
                            <li>Access to our Portfolio Research</li>
                            <li>Free Build Updates</li>
                            <li>Personalized 24/7 Support</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
          {{-- </div> --}}
        </div>
      </div>

      {{-- Learn More --}}
    <div class="container">
        <div class="learn-more">
            <h2>{{ trans('blogs.blog-title') }}</h2>

            <div class="learn-more-card">
                
                    <div class="card">
                        <img class="card-img-top" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card1.svg" alt="Card image cap">
                        <div class="card-body">
                            <p>Arbitrage Recommendation from Orrbit</p>
                            <div class="card-bottom">
                                <img src="{{url('/')}}/webpages/images/swap-arbitrage/johanna.svg" alt="">
                                <div class="card-name ml-3 mt-2">
                                    <p class="mb-0"><strong>By Johanna</strong></p>
                                    <p>10 mins read</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                    <div class="card">
                        <img class="card-img-top" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card2.svg" alt="Card image cap">
                        <div class="card-body">
                            <p>Federal Reserve Orders New Round of Stress Tests</p>
                            <div class="card-bottom">
                                <img src="{{url('/')}}/webpages/images/swap-arbitrage/ariella.svg" alt="">
                                <div class="card-name ml-3 mt-2">
                                    <p class="mb-0"><strong>By Johanna</strong></p>
                                    <p>10 mins read</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                    <div class="card">
                        <img class="card-img-top" src="{{url('/')}}/webpages/images/swap-arbitrage/learn-more-card3.svg" alt="Card image cap">
                        <div class="card-body">
                            <p>Currency Pair Definition</p>
                            <div class="card-bottom">
                                <img src="{{url('/')}}/webpages/images/swap-arbitrage/riel.svg" alt="">
                                <div class="card-name ml-3 mt-2">
                                    <p class="mb-0"><strong>By Johanna</strong></p>
                                    <p>10 mins read</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>

    

      
    {{-- End of Section 3 --}}
@endsection