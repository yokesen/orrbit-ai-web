@extends('webpages.templates.master')

@section('title', $title)
@section('description', $description)
@section('imageog', $imageog)

@section('content')

  {{-- Homepage Jumbotron --}}
    <div class="jumbotron jumbotron-custom" style="background: url({{url('/')}}/webpages/images/homepage/new-bg.png) ">
      <div class="container">
        <div class="row">
          <div class="col-md col-centered">
            <div>
              <h1>{{ trans('main-header.header-title') }}</h1>
              <h3>
                {{ trans('main-header.header-caption') }}
              </h3>
              <button type="button" class="btn" id="learn-more-btn">{{ trans('main-header.header-button') }}</button>
            </div>
          </div>
          <div class="col-md col-centered">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe
                class="embed-responsive-item"
                style="border-radius: 20px; border:transparent"
                frameborder="0"
                src="https://drive.google.com/file/d/1uFMzx3Y98X_k299IdGT2H3EzgLrxjLU9/preview"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>


    {{-- Try Demo --}}
    <div class="container try-demo" id="try-demo">
      <h2>{{ trans('arbitrage-explanation.arbitrage-explanation-title') }}</h2>
      <h3>{{ trans('arbitrage-explanation.arbitrage-explanation-caption') }}</h3>

      <div class="demo-icon-container">
        <div class="row">
          <div class="col-md-4 demo-icon">
            <img src="{{url('/')}}/webpages/images/homepage/hedged.svg" alt="">
            <p>{{ trans('arbitrage-explanation.arbitrage-explanation-1') }}</p>
          </div>
          <div class="col-md-4 demo-icon">
            <img src="{{url('/')}}/webpages/images/homepage/risk.svg" alt="">
            <p>{{ trans('arbitrage-explanation.arbitrage-explanation-2') }}</p>
          </div>
          <div class="col-md-4 demo-icon">
            <img src="{{url('/')}}/webpages/images/homepage/monitor.svg" alt="">
            <p>{{ trans('arbitrage-explanation.arbitrage-explanation-3') }}</p>
          </div>
        </div>
      </div>

      <div class="try-demo-btn">
        <button type="button" class="btn" data-toggle="modal" data-target="#loginModal">{{ trans('arbitrage-explanation.arbitrage-explanation-button') }}</button>
      </div>
    </div>

    <section class="pricing py-5">
      <div class="container">
        <div class="row">

          {{-- Plan 1 --}}
          <div class="col-lg-4">
            <div class="card mb-5 mb-lg-0">
              <div class="card-body">
                <h3 class="card-title text-center">1 month</h3>
                {{-- <h2 class="card-price text-center">${{$robots->robotPrice3}}</h2> --}}
                <h2 class="card-price text-center">
                  @if ($locale == 'cn')
                    {{'¥' . round($robots->robotPrice3 * $currency->CNY)}}
                  @elseif($locale == 'id')
                    <?php $harga = round($robots->robotPrice3 * $currency->IDR) ?>
                    {{'Rp ' . number_format($harga,0,',','.')}}
                  @else
                    {{'$'.$robots->robotPrice3}}
                  @endif  
                </h2>
                <a href="{{ url('add-to-cart/'.$robots->id.'/1')}}" class="btn btn-block text-uppercase">buy now</a>
                <hr>
                <ul class="fa-ul">
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                </ul>
              </div>
            </div>
          </div>

           {{-- Plan 3 --}}
           <div class="col-lg-4 plan3">
            <div class="card mb-5 mb-lg-0">
              <div class="card-header">BEST DEAL!</div>
              <div class="card-body">
                <h3 class="card-title text-center">1 year</h3>
                <h2 class="card-price text-center">
                  @if ($locale == 'cn')
                    {{'¥' . round($robots->robotPrice1 * $currency->CNY)}}
                  @elseif($locale == 'id')
                  <?php $harga = round($robots->robotPrice1 * $currency->IDR) ?>
                  {{'Rp ' . number_format($harga,0,',','.')}}
                  @else
                    {{'$'.$robots->robotPrice1}}
                  @endif 
                </h2>
                <a href="{{ url('add-to-cart/'.$robots->id.'/12')}}" class="btn btn-block text-uppercase">buy now</a>
                <hr>
                <ul class="fa-ul">
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                </ul>
              </div>
            </div>
          </div>

          {{-- Plan 2 --}}
          <div class="col-lg-4">
            <div class="card mb-5 mb-lg-0">
              <div class="card-body">
                <h3 class="card-title text-center">3 months</h3>
                <h2 class="card-price text-center">
                  @if ($locale == 'cn')
                    {{'¥' . round($robots->robotPrice2 * $currency->CNY)}}
                  @elseif($locale == 'id')
                  <?php $harga = round($robots->robotPrice2 * $currency->IDR) ?>
                  {{'Rp ' . number_format($harga,0,',','.')}}
                  @else
                    {{'$'.$robots->robotPrice2}}
                  @endif   
                </h2>
                <a href="{{ url('add-to-cart/'.$robots->id.'/3')}}" class="btn btn-block text-uppercase">buy now</a>
                <hr>
                <ul class="fa-ul">
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Expert Advisors </li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Full User Guide with many Best Practise Tips and Trick</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Orrbit Portfolio Research and Record</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Free Build Updates</li>
                  <li><span class="fa-li"><i class="fas fa-star-of-life"></i></span>Personalized 24/7 Support</li>
                </ul>
              </div>
            </div>
          </div>

         
        </div>
      </div>
    </section>

    {{-- Live Trading --}}
    {{-- <div class="container live-trading">
      <div class="card">

        <h3 class="trading-title">
         live trading results
        </h3>

        <div class="trading-form">
          <form>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Starting Balance</label>
              <div class="col-sm-8">
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Monthly Gains</label>
              <div class="col-sm-8">
                <input type="text" class="form-control">
              </div>
            </div>
          </form>
        </div>

        <div class="trading-btn">
          <button type="button" class="btn">calculate</button>
        </div>


        <div class="trading-result">
          <div class="d-flex mr-5">
            <p class="mr-5">12 months</p>
            <p>-</p>
          </div>
          <div class="d-flex ml-5">
            <p class="mr-5">24 months</p>
            <p>-</p>
          </div>
        </div>
      </div>
    </div> --}}


  {{-- Market Summary --}}
    <div class="market-card container">
      <div class="card big-card">
        <h3 class="market-title">Market Summary</h3>
        <div class="chart">
          <canvas id="myChart" width="400" height="200"></canvas>
        </div>

        <div class="currency row">
          <div class="card col-md-6">
            <div class="card-body">
              <div class="mata-uang-img mr-3" style="position: relative">
                <img src="{{url('/')}}/webpages/images/homepage/eurusd.png" class="img-fluid" alt="">
                {{-- <img src="{{url('/')}}/webpages/images/homepage/eur.svg" class="img-fluid" alt="" style="position: absolute; top: 0; left:0; z-index: 2"> --}}
                {{-- <img src="{{url('/')}}/webpages/images/homepage/usd.svg" class="img-fluid" alt="" style="position: absolute; bottom: 0; right:0; z-index: 1" > --}}
              </div>
              <div class="mata-uang">
                <div class="mata-uang-symbol">EURUSD</div>
                <div class="mata-uang-desc">Euro/U.S Dollar</div>
              </div>
              <div class="ml-auto ask" id="xauusd"></div>
              <div class="ml-5 ask-percent ask-percent">
                <div>−0.01%</div>
                <div>0.00002</div>
              </div>
            </div>
          </div>
         
          <div class="card col-md-6">
            <div class="card-body">
              <div class="mata-uang-img mr-3" style="position: relative">
                <img src="{{url('/')}}/webpages/images/homepage/audusd.png" class="img-fluid" alt="">
                {{-- <img src="{{url('/')}}/webpages/images/homepage/aud.svg" class="img-fluid" alt="" style="position: absolute; top: 0; left:0; z-index: 2"> --}}
                {{-- <img src="{{url('/')}}/webpages/images/homepage/usd.svg" class="img-fluid" alt="" style="position: absolute; bottom: 0; right:0; z-index: 1"> --}}
              </div>
              <div class="mata-uang">
                <div class="mata-uang-symbol">AUDUSD</div>
                <div class="mata-uang-desc">Australian Dollar/U.S.D.</div>
              </div>
              <div class="ml-auto ask" id="xagusd"></div>
              <div class="ml-5 ask-percent">
                <div>−0.07%</div>
                <div>0.00048</div>
              </div>
            </div>
          </div>

          <div class="card col-md-6">
            <div class="card-body">
              <div class="mata-uang-img mr-3" style="position: relative">
                <img src="{{url('/')}}/webpages/images/homepage/gbpusd.png" class="img-fluid" alt="">
                {{-- <img src="{{url('/')}}/webpages/images/homepage/gbp.svg" class="img-fluid" alt="" style="position: absolute; top: 0; left:0; z-index: 2"> --}}
                {{-- <img src="{{url('/')}}/webpages/images/homepage/usd.svg" class="img-fluid" alt="" style="position: absolute; bottom: 0; right:0; z-index: 1"> --}}
              </div>
              <div class="mata-uang">
                <div class="mata-uang-symbol">GBPUSD</div>
                <div class="mata-uang-desc">British Pound / U.S. Dollar</div>
              </div>
              <div class="ml-auto ask" id="goldx"></div>
              <div class="ml-5 ask-percent">
                <div>+0.07%</div>
                <div>+0.0009</div>
              </div>
            </div>
          </div>

          <div class="card col-md-6">
            <div class="card-body">
              <div class="mata-uang-img mr-3" style="position: relative">
                <img src="{{url('/')}}/webpages/images/homepage/audusd.png" class="img-fluid" alt="">
                {{-- <img src="{{url('/')}}/webpages/images/homepage/usd.svg" class="img-fluid" alt="" style="position: absolute; top: 0; left:0; z-index: 2"> --}}
                {{-- <img src="{{url('/')}}/webpages/images/homepage/cad.svg" class="img-fluid" alt="" style="position: absolute; bottom: 0; right:0; z-index: 1"> --}}
              </div>
              <div class="mata-uang">
                <div class="mata-uang-symbol">USDCAD</div>
                <div class="mata-uang-desc">U.S. Dollar/Canadian Do...</div>
              </div>
              <div class="ml-auto ask" id="silverx"></div>
              <div class="ml-5 ask-percent">
                <div>+0.01%</div>
                <div>0.00007</div>
              </div>
            </div>
          </div>

          <div class="card col-md-6">
            <div class="card-body">
              <div class="mata-uang-img mr-3" style="position: relative">
                <img src="{{url('/')}}/webpages/images/homepage/usdjpy.png" class="img-fluid" alt="">
                {{-- <img src="{{url('/')}}/webpages/images/homepage/usd.svg" class="img-fluid" alt="" style="position: absolute; top: 0; left:0; z-index: 2"> --}}
                {{-- <img src="{{url('/')}}/webpages/images/homepage/jpy.svg" class="img-fluid" alt="" style="position: absolute; bottom: 0; right:0; z-index: 1"> --}}
              </div>
              <div class="mata-uang">
                <div class="mata-uang-symbol">USDJPY</div>
                <div class="mata-uang-desc">U.S. Dollar/Japanese Yen</div>
              </div>
              <div class="ml-auto ask" id="goldx"></div>
              <div class="ml-5 ask-percent">
                <div>−0.13%</div>
                <div>−0.135</div>
              </div>
            </div>
          </div>

          <div class="card col-md-6">
            <div class="card-body">
              <div class="mata-uang-img mr-3" style="position: relative">
                <img src="{{url('/')}}/webpages/images/homepage/usdchf.png" class="img-fluid" alt="">
                {{-- <img src="{{url('/')}}/webpages/images/homepage/usd.svg" class="img-fluid" alt="" style="position: absolute; top: 0; left:0; z-index: 2"> --}}
                {{-- <img src="{{url('/')}}/webpages/images/homepage/chf.svg" class="img-fluid" alt="" style="position: absolute; bottom: 0; right:0; z-index: 1"> --}}
              </div>
              <div class="mata-uang">
                <div class="mata-uang-symbol">USDCHF</div>
                <div class="mata-uang-desc">U.S. Dollar / Swiss Franc</div>
              </div>
              <div class="ml-auto ask" id="silverx"></div>
              <div class="ml-5 ask-percent">
                <div>+0.02%</div>
                <div>+0.00027</div>
              </div>
            </div>
          </div>

        </div>

        <div class="show-more">Show More</div>
      </div>
    </div>

    {{-- Reason you need to join us now --}}
    <div class="container reason">
      <h2 class="reason-title">{{ trans('reason-section.reason-section-title') }}</h2>
      <div class="reason-list" >
        <div class="card">
          <div class="card-body">
            <img src="{{url('/')}}/webpages/images/homepage/lowestrisk.svg" alt="" />
            <div class="card-content">
              <h3>{{ trans('reason-section.reason-section-1') }}</h3>
              <p>
                {{ trans('reason-section.reason-section-1-caption') }}
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <img src="{{url('/')}}/webpages/images/homepage/consistent.svg" alt="" />
            <div class="card-content">
              <h3>{{ trans('reason-section.reason-section-4') }}</h3>
              <p>
               {{ trans('reason-section.reason-section-4-caption') }}
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <img src="{{url('/')}}/webpages/images/homepage/direction.svg" alt="" />
            <div class="card-content">
              <h3>{{ trans('reason-section.reason-section-2') }}</h3>
              <p>
                {{ trans('reason-section.reason-section-2-caption') }}
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <img src="{{url('/')}}/webpages/images/homepage/autopilot.svg" alt="" />
            <div class="card-content">
              <h3>{{ trans('reason-section.reason-section-5') }}</h3>
              <p>
                {{ trans('reason-section.reason-section-5-caption') }}
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <img src="{{url('/')}}/webpages/images/homepage/ea.svg" alt="" />
            <div class="card-content">
              <h3>{{ trans('reason-section.reason-section-3') }}</h3>
              <p>
                {{ trans('reason-section.reason-section-3-caption') }}
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <img src="{{url('/')}}/webpages/images/homepage/flextask.svg" alt="" />
            <div class="card-content">
              <h3>{{ trans('reason-section.reason-section-6') }}</h3>
              <p>
                {{ trans('reason-section.reason-section-6-caption') }} 
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

{{-- ========================================================= --}}


    <!-- How it Works -->
    <div class="how-it-work">
      <div class="container">
        <h2>{{ trans('how-it-works-section.how-it-works-title') }}</h2>
          <div class="step1 row" style="margin-bottom: 5em;" >
            <div class="col-md-6" >
              <div class="step1-img">
                <img src="{{url('/')}}/webpages/images/homepage/step1.png" class="img-fluid" alt="">
              </div>
            </div>
              <div class="step col-md-6">
                <p>{{ trans('step-three-title.step-one-title') }}</p>
                <ul>
                  <li>
                    {{ trans('step-three-title.step-one-caption-1') }}
                  </li>
                  <br />
                  <li>
                    {{ trans('step-three-title.step-one-caption-2') }}
                  </li>
                </ul>
              </div>
          </div>
          
          <div class="step2 row" style="margin-bottom: 5em;" >
            <div class="col-md-6 step2-img">
              <div class="step1-img">
                <img src="{{url('/')}}/webpages/images/homepage/step2.png" class="img-fluid" alt="">
              </div>
            </div>
              <div class="step col-md-6 step2-text">
                <p>{{ trans('step-three-title.step-two-title') }}</p>
                <ul>
                  <li>
                    {{ trans('step-three-title.step-two-caption-1') }}
                  </li>
                  <br />
                  <li>
                    {{ trans('step-three-title.step-two-caption-2') }}
                  </li>
                </ul>
              </div>
          </div>
          
          <div class="step1 row" >
            <div class="col-md-6" >
              <div class="step1-img">
                <img src="{{url('/')}}/webpages/images/homepage/step3.png" class="img-fluid" alt="">
              </div>
            </div>
              <div class="step col-md-6" >
                <p>{{ trans('step-three-title.step-three-title') }}</p>
                <ul>
                  <li>
                    {{ trans('step-three-title.step-three-caption-1') }}
                  </li>
                  <br />
                  <li>
                    {{ trans('step-three-title.step-three-caption-2') }}
                  </li>
                </ul>
              </div>
          </div>
      </div>
    </div>
    <!-- End of How it works -->

  <!-- Help Section -->
  <div class="container">
    <div class="help-section row">
      <div class="help-item col-md-4">
        <div class="help-icon">
          <img src="{{url('/')}}/webpages/images/homepage/help.svg" alt="" />
        </div>
        <h3 class="help-title">{{ trans('helps-section.help-title') }}</h3>
        <p>
          {{ trans('helps-section.help-caption') }}
        </p>
      </div>
      <div class="help-item col-md-4">
        <div class="help-icon">
          <img src="{{url('/')}}/webpages/images/homepage/support.svg" alt="" />
        </div>
        <h3 class="help-title">{{ trans('helps-section.hotline-title') }}</h3>
        <p>
          {{ trans('helps-section.hotline-caption') }}
        </p>
      </div>
      <div class="help-item col-md-4">
        <div class="help-icon">
          <img src="{{url('/')}}/webpages/images/homepage/discussion.svg" alt="" />
        </div>
        <h3 class="help-title">{{ trans('helps-section.dicscussion-title') }}</h3>
        <p>
          {{ trans('helps-section.dicscussion-caption') }}
        </p>
      </div>
    </div>
  </div>
    <!-- End of Help Section -->

    <!-- Testimonial -->

  <div class="testimoni">
    <h2>{{ trans('testimoni-section.testimoni-section-title') }}</h2>
    <div class="owl-carousel">
      <div class="testimoni-card">
        <div class="card">
          <div class="testimoni-img">
            <img src="{{url('/')}}/webpages/images/homepage/testi1.svg" class="img-fluid" alt="" />
          </div>
          <div class="testimoni-kanan">
            <p class="the-testimoni">
              "As a beginner in this field, it really helps me to get passive income without having to understand and learn the product until I really understand, after the portfolio is prepared everything is done by the software, everything is completely automated, thanks."
            </p>
            <h3 class="testimoni-name">Edward</h3>
            <p class="testimoni-title">Legal Consultant, Indonesia</p>
          </div>
        </div>
      
      </div>
  
  
  
  
      <div class="testimoni-card">
  
        <div class="card">
          <div class="testimoni-img">
            <img src="{{url('/')}}/webpages/images/homepage/testi2.svg" class="img-fluid" alt="" />
          </div>
          <div class="testimoni-kanan">
            <p class="the-testimoni">
              "This is good for me, because if I enter long and short positions of the same size on two different brokerage accounts (with positive swap differences), my positions will be fully protected and you will make money consistently."
            </p>
            <h3 class="testimoni-name">Jonathan </h3>
            <p class="testimoni-title">Legal Consultant, Indonesia</p>
          </div>
        </div>
      
      </div>
  
  
  
  
      <div class="testimoni-card">
  
        <div class="card">
          <div class="testimoni-img">
            <img src="{{url('/')}}/webpages/images/homepage/testi2.svg" class="img-fluid" alt="" />
          </div>
          <div class="testimoni-kanan">
            <p class="the-testimoni">
              "It's great for me as a trader, all trades are permanently hedged. There is no directional trading risk, the return projections are very reliable as the broker rarely changes their exchange rates, thank you."
            </p>
            <h3 class="testimoni-name">Michael Wong</h3>
            <p class="testimoni-title">Legal Consultant, Indonesia</p>
          </div>
        </div>
      
      </div>
     
  
      
  
    </div>
  </div>

  
    <!-- End of Testimonial -->

@endsection