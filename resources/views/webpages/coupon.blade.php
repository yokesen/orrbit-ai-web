@extends('webpages.templates.master')
@section('title','Coupon | Overnight Arbitrage')

@section('content')
    <div class="container">
        <h2 class="my-5">Promo & Coupon</h2>
        <div class="row justify-content-center">
            <div class="col-12 mb-3">
                <div class="input-group input-group-lg mb-3 text-center">
                    <input type="text" name="voucher" id="kodeVoucher" class="form-control" placeholder="Masukkan kode voucher">
					<div class="input-group-append">
					    <button class="btn check-code btn-sm">Check Code</button>
                    </div>
                </div>
                <div class="container">
                  <p id="voucherDetail" style="display: none;"></p>
                </div>
            </div>
        </div>

        <h3>You can also use this promotion code</h3>

        <div class="mb-5">
        @forelse ($voucher as $vouchers)
         <?php
            $fungsi=$vouchers->fungsiVoucher;
            $data=str_replace(";",", ",$fungsi);
            if($data==null){
                $data="all service";
            }
         ?>
            <div class="cart-block mt-3 p-3">
                <div class="row">
                <div class="col-10">
                        <h4>{{strtoupper($vouchers->kodeVoucher)}}</h4>
                        <p>Discount {{$vouchers->diskonVoucher}}% for {{$data}}</p>
                    </div>
                    <div class="col-2 align-self-center">
                        <div class="input-group-append">
                        <button class="btn use-code btn-sm" onclick="window.location.href='{{ route('useVoucher', ['kode' => $vouchers->kodeVoucher]) }}'">Use code</button>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="cart-block mt-3 p-3">
                <div class="row">
                    <div class="col-12">
                        <p>Belum ada voucher tersedia</p>
                    </div>
                </div>
            </div>
        @endforelse
        </div>
    </div>
<br>
@endsection