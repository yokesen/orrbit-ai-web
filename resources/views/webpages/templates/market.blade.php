<script>

    // for(let i = 0; i<12; i++) {randomData.push(parseInt(Math.random() * 101))}
    
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'line', // also try bar or other graph types

      // The data for our dataset
      data: {
        labels: [],
        // labels: ["Jun 2016", "Jul 2016", "Aug 2016", "Sep 2016", "Oct 2016", "Nov 2016", "Dec 2016", "Jan 2017", "Feb 2017", "Mar 2017", "Apr 2017", "May 2017"],
        // Information about the dataset
        datasets: [{
          label: "Rainfall",
          backgroundColor: 'rgba(251, 184, 75, 0.3)',
          borderColor: ' rgba(251, 183, 75, 0.857)',
          // data: [26.4, 39.8, 66.8, 66.4, 40.6, 55.2, 77.4, 120, 57.8, 76, 110.8, 142.6],
          data: [],
          pointRadius: 0
        }]
      },

      // Configuration options
      options: {
        tooltips: {enabled: false},
        hover: {mode: null},
        legend: {
          display: false
        },
        scales: {
              xAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    display: false
                }
              }],
              yAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                    display: false
                }
              }]
        }
      }
    });


// ==============================================================================================

    var dataLabel = []
    var dataChart = []
    getSocket = () => {
    const socket = new WebSocket("wss://rh-info.risehills.com/v1/app/qws")
      this.socket = socket
      // console.log(this.socket)
  
      socket.addEventListener("open",function( event ) {
          socket.send(`{"Cmd":"Subscribe","Data":{"symbols":["XAUUSD,XAGUSD"]}}`);
      });
      
      socket.addEventListener("message",( event ) => {
          // console.log(event.data)
          const { Cmd, Data } = JSON.parse(event.data)
         
          if (Cmd === "Quote") {
              const {symbol, ask, bid, time} = Data
              
              if (symbol == "XAUUSD.ht") {
                dataLabel.push(time)
                dataChart.push(ask)
                chart.data.labels = dataLabel
                chart.data.datasets[0].data = dataChart
                // console.log(chart.data.labels)
                // console.log(chart.data.datasets[0].data)
                // chart.data.labels = `${time}`
                // chart.data.datasets[0].data = ask
                // chart.data.label.push(`${time}`)
                // chart.data.datasets[0].data.push(ask)
                chart.update()
                $('#xauusd').text(ask)
              }
              
              if (symbol == "XAGUSD.ht") {
                $('#xagusd').text(ask)
              }
              if (symbol == "GOLDx") {
                $('#goldx').text(ask)
              }
              if (symbol == "SILVERx") {
                $('#silverx').text(ask)
              }
          }
      })
  
      socket.onclose = function(event) {
          if (event.wasClean) {
              getSocket()
              // console.log(event)
              console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
          } else {
              console.log('[close] Connection died');
          }
      };
}
getSocket()
</script>