<nav class="navbar navbar-expand-xl navbar-custom sticky-top">

  @if (!empty(CRUDBooster::myName()))
    {{-- Navbar Mobile After Login --}}
    {{-- Burger Menu --}}
    <button
    class="navbar-toggler"
    type="button"
    data-toggle="collapse"
    data-target="#navbarText"
    aria-controls="navbarText"
    aria-expanded="false"
    aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon">
        <i class="fas fa-bars" style="font-size: 2rem; color: white"></i>
      </span>
    </button>

    {{-- Logo / Brand --}}
    <a class="navbar-brand" href="{{route('homePage')}}">
      <img src="{{url('/')}}/webpages/images/orrbit-logo.svg" alt="">
    </a>

    {{-- Burger User --}}
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarText"
      aria-controls="navbarText"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon">
        <i class="far fa-user-circle" style="font-size: 2rem; color: white"></i>
      </span>
    </button>
  @else
    {{-- Navbar Mobile Before Login --}}
    {{-- Logo / Brand --}}
    <a class="navbar-brand" href="{{route('homePage')}}">
      <img src="{{url('/')}}/webpages/images/orrbit-logo.svg" alt="">
    </a>

    {{-- Burger Menu --}}
    <button
    class="navbar-toggler"
    type="button"
    data-toggle="collapse"
    data-target="#navbarText"
    aria-controls="navbarText"
    aria-expanded="false"
    aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon">
        <i class="fas fa-bars" style="font-size: 2rem; color: white"></i>
      </span>
    </button>
  @endif
    

    

    {{-- Navigation Item --}}
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link {{set_active('homePage')}}" href="{{route('homePage')}}">{{ trans('navbar.nav-home') }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{set_active('swapPage')}}" href="{{route('swapPage')}}"> {{ trans('navbar.nav-swap') }} </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{set_active('techPage')}}" href="{{route('techPage')}}">{{ trans('navbar.nav-tech') }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{set_active('pricingPage')}}" href="{{route('pricingPage')}}">{{ trans('navbar.nav-price') }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{set_active('blogPage')}}" href="{{route('blogPage')}}">{{ trans('navbar.nav-blog') }}</a>
        </li>

        @if (!empty(CRUDBooster::myName()))
        {{-- If already Login show these menu --}}
        <li class="nav-item dropdown dropdown-custom">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{CRUDBooster::myName()}}</a>
          <div class="dropdown-menu">
              <a class="dropdown-item" href="{{route('collectionPage')}}">Produk</a>
              <a class="dropdown-item" href="{{route('historyPage')}}">Riwayat Pembelian</a>
              <a class="dropdown-item" href="{{route('couponPage')}}">Redeem Code</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{route('profile')}}">Pengaturan</a>
              <a class="dropdown-item" href="{{route('userLogout')}}">Logout</a>
          </div>
        </li>
        @else
        {{-- if not login yet show this menu --}}
        <li class="nav-item">
          <a class="nav-link login" data-toggle="modal" data-target="#loginModal"><i class="fas fa-user mr-1"></i> {{ trans('navbar.nav-login') }}</a>
        </li>
        @endif
        
        {{-- Cart Menu --}}
        <li class="nav-item">
          <a href="{{route('cartPage')}}" class="nav-link" style="color: #FBB84B"><i class="fas fa-shopping-cart"></i><sup class="icon-cart" style="font-size: 16px"> {{Cart::count() >0 ? Cart::count() : ''}}</sup></a>
        </li>

        {{-- Language Menu --}}
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ $currentLanguage->name }}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            @foreach ($altLocalizedUrls as $alt)
                  <a class="dropdown-item" style="text-transform: capitalize" href="{{ $alt['url'] }}" hreflang="{{ $alt['locale'] }}">{{ $alt['name'] }}</a>
            @endforeach
          </div>
        </li>
      
      </ul>
    </div>
</nav>

<!-- Modal Login -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body p-5">
        @if ( Session::get('message') != '' )
          <div class='alert alert-warning'>
              {{ Session::get('message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h2 style="font-weight: 700 !important">Login</h2>
        <p>New user? Let’s create a <a href="{{route('registerPage')}}">new one</a></p>

      

        <form style="margin: 3em auto" action="{{ route('userLogin') }}" method="post" autocomplete="off">
          <div class="form-group">
            <label for="inputEmail"><strong>Email Address</strong></label>
            <input type="email" class="form-control input-login" id="inputEmail" name="email" aria-describedby="emailHelp">
          </div>
          <div class="form-group">
            <label for="inputPassword"><strong>Password</strong></label>
            <input type="password" class="form-control input-login" name="password" id="inputPassword">
          </div>
          @csrf
          <button type="submit" class="btn btn-lg btn-block login-btn">LOGIN</button>

          <h3 class="text-center my-4">Or</h3>
          <button type="button" class="btn btn-lg btn-block login-fb"><i class="fab fa-facebook-f mr-3"></i> Login with Facebook</button>
          <button type="button" class="btn btn-lg btn-block login-google"><i class="fab fa-google mr-3"></i>Login with Google</button>
        </form>
      </div>
    </div>
  </div>
</div>
