<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Primary Meta Tags -->
    <title>@yield('title')</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{url('/')}}/webpages/images/orrbit-favicon.ico">
    <meta name="title" content="@yield('title')">
    <meta name="description" content="@yield('description')">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:image" content="@yield('imageog')">

    {{-- Style CSS --}}
    <link rel="stylesheet" href="{{url('/')}}/webpages/style.css" />
    <link rel="stylesheet" href="{{url('/')}}/dashboard/dashboard.css" />

    {{-- Brand Font --}}
    <link
      href="https://fonts.googleapis.com/css2?family=Reem+Kufi&display=swap"
      rel="stylesheet"
    />

    {{-- Content Font --}}
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;700;900&display=swap" rel="stylesheet">

    {{-- Bootstrap CSS --}}
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
      crossorigin="anonymous"
    />

    {{-- Font Awesome Icon --}}
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.14.0/css/all.css"
      integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc"
      crossorigin="anonymous"
    />

    {{-- Owl Carousel --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    {{-- Bootstrap JS --}}
    
    <!-- <script
      
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script> -->


    {{-- Chart JS --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    {{-- Owl Carousel  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
      integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
      integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
      crossorigin="anonymous"
    ></script>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
  <body>
    @include('webpages.templates.nav')  
    @yield('content')
    <!-- Footer -->
    <div class="footer"></div>
  
    
    @include('webpages.templates.market')


  
    
    <script>
      // Testimony Carousel Setting
      $(document).ready(function(){
        $(".owl-carousel").owlCarousel({
          center: true,
          dots:true,
          autoplay:true,
          loop:true,
          margin:10,
          responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                  },
                600:{
                    items:1,
                    stagePadding: 100
                },
                768: {
                    items: 1,
                    stagePadding: 100
                },
                1280:{
                    items:2,
                    stagePadding:100
                },
                1400: {
                    items:3,
                    stagePadding:100
                }
            }
        });
      });
    </script>

    @include('script.learnMore')
    @include('script.copyClipboard')

    {{-- Show Modal jika gagal login --}}
    @if (Session::get('message') != '')
    <script>
      $(function() {
        console.log('masuk')
          $('#loginModal').modal('toggle');
      });
      </script>
    @endif

    @yield('jsonpage')
  </body>
</html>
