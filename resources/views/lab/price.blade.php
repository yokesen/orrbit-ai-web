<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <p>price</p>
    <script src="/socket.io/socket.io.js"></script>
    <script type="text/javascript">

    const socket = createSocketClient(
      "wss://rh-info.risehills.com/v1/app/qws"
    );

    this.socket = socket;

    socket.addEventListener("open", function (event) {
      socket.send(`{"Cmd":"Subscribe","Data":{"symbols":["XAUUSD,XAGUSD"]}}`);
    });

    socket.addEventListener("message", (event) => {

      const { Cmd, Data } = JSON.parse(event.data);

      if (Cmd === "Quote") {
        const { symbol, ask, bid } = Data;
        const [type, name] = symbol.split(".");
        let Name = name && name.toUpperCase();
        if (type == "GOLDx" || type == "SILVERx") {
          Name = "RH";
        }
        if (type != "spread") {
          if (!this.symbols[Name]) {
            this.symbols[Name] = "";
          }

            if (type === "XAUUSD" || type === "GOLD" || type === "GOLDx")  {
              this.symbols[Name].GOLD = ask;
            } else if (
              type === "XAGUSD" ||
              type === "SILVER" ||
              type === "SILVERx"
            ) {
              this.symbols[Name].SILVER = ask;
            }

            const findIndex = this.symbolList.findIndex(
              (item) => item.name === Name
            );
            if (findIndex > -1) {
              this.symbolList.splice(findIndex, 1, {
                name: Name,
                ...this.symbols[Name],
              });
            } else {
              this.symbolList.push({
                name: Name,
                ...this.symbols[Name],
              });
            }
            this.symbolList = this.symbolList.sort((a, b) => {
              return a.name === "RH" ? -1 : 1;
            });
          }
        }
      });

    </script>
  </body>
</html>
