{{-- Copy to Clipboard --}}
<script>
    $('.license-btn').tooltip({
      trigger: 'click',
      placement: 'bottom'
    });

    function setTooltip(message) {
      $('.license-btn').tooltip('hide')
        .attr('data-original-title', message)
        .tooltip('show');
    }

    function hideTooltip() {
      setTimeout(function() {
        $('.license-btn').tooltip('hide');
      }, 1000);
    }

    function copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
      setTooltip('Copied!')
      hideTooltip()
      // $('[data-toggle="tooltip"]').tooltip()
    }

  </script>