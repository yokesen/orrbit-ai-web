@extends("crudbooster::admin_template")
@section("content")
  

  @foreach($robots as $robot)
  <div class="row">
    <div class="col-md-12">
      <h3>Pemesanan Robot</h3>
      <p>Step 1 (of 2) : order Robot</p>
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <div class="image-150">
            <img class="storePayment1-img" src="{{url('/')}}/{{$robot->pictures}}" alt="" width="100%"/>
          </div>
        </div>
        <div class="box-body">
          <h4>Robot: {{$robot->productName}}</h4>
          <br>
          @if ($robot->periodMonth == 1)
            <h4>Periods: {{$robot->periodMonth}} Month</h4>
          @else
            <h4>Periods: {{$robot->periodMonth}} Months</h4>
          @endif
          <br>
          <h4>Dapatkan keuntungan dengan sistem Swap Arbitrage bersama Orrbit AI</h4>
        </div>
        <div class="box-footer">
          <a href="{{route('storePaymentStep2',['id'=>$robot->id])}}" class="btn btn-danger btn-lg btn-block">Order Robot</a>
        </div>
      </div>
    </div>  
  </div>

  @endforeach
@endsection