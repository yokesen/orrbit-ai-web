@extends('crudbooster::admin_template')


@section('content')

  <div class="report-wrapper">
    <div class="container report-title-wrapper">
      <h1 class="report-detail-title">Report</h1>
      <h4 class="sub-title">Product reported: xxxxxxx</h4>
    </div>
    <div class="container card-container">
      <div class="card card-chat">
        <div class="card-body">
          <img class="photo-user-report"  src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg" alt="">
          <h4 class="card-title card-content-name">Name</h4>
          <h4 class="card-title mb-2">UserID</h4>
          <div class="chat-content">
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>          
          </div>
        </div>
      </div>
    </div>
  </div>
    
@endsection