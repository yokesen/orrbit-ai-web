@extends('crudbooster::admin_template')

@section('content')
<div class="container-fluid home-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid box-home">
        <!-- /.box-header -->
        <div class="box-body box-body-home">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
              <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner">
              <div class="item active">
                <img class="home-banner"  src="{{url('/')}}/webpages/images/user/home-banner.jpg" alt="First slide">

                <div class="carousel-caption">
                  First Slide
                </div>
              </div>
              <div class="item">
                <img class="home-banner" src="{{url('/')}}/webpages/images/user/home-banner.jpg" alt="Second slide">

                <div class="carousel-caption">
                  Second Slide
                </div>
              </div>
              <div class="item">
                <img class="home-banner" src="{{url('/')}}/webpages/images/user/home-banner.jpg" alt="Third slide">

                <div class="carousel-caption">
                  Third Slide
                </div>
              </div>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
              <span class="fa fa-angle-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
              <span class="fa fa-angle-right"></span>
            </a>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
    <div class="container-fluid grid-home">
      <div class="left-grid col-lg-6 ">
        <div class="col-md-12 home-item2">
          
            <p>Check your product update</p>
            <div class="box-body">
              {{date('d-m-Y H:i')}}
            </div>
          
        </div>
        <div class="col-md-12 home-item4">
          
            <p>
              Share Your Referal Key
              <img class="arrow-icon" src="{{ asset('/dashboard/images/Vector(1).svg')}}">
            </p>
            <h3>7x183HZN</h3>
          </div>
        
      </div>
      <div class="right-grid col-lg-6">
        <div class="col-md-12 home-item3">
          
            <p>Weekly report</p>
            <div class="graphic-image-wrapper">
              <img class="graphic-image" src="{{ asset('/dashboard/images/graphic-home.svg')}}"> 
            </div>
          
        </div>
      </div>
    </div>
  </div>
</div>


@endsection