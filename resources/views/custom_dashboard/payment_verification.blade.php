@extends("crudbooster::admin_template")

{{-- @section() --}}

@section("content")
{{-- table start --}}
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <div class="box-tools">
          <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>ID</th>
            <th>Customer Name</th>
            <th>Product Name</th>
            <th>Period</th>
            <th>Timecode</th>
            <th>Proof of Payment</th>
            <th></th>
          </tr>
          
          @foreach ($getData as $datum)   
            @if ($datum->payment == 1)
            
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            @else
            <tr>
              <td>{{$datum->id}}</td>
              <td>{{$datum->name}} {{$datum->lastName}}</td>
              <td>{{$datum->productName}}</td>
              <td>
                @if ($datum->periodMonth > 1)
                  {{$datum->periodMonth}} Months
                @else
                  {{$datum->periodMonth}} Month
                @endif
              </td>
              <td>{{$datum->created_at}}</td>
              <td><img class="img-payment-verification" src="{{asset($datum->struck)}}" alt=""></td>
              <td><a href="{{route('paymentConfirmation',['id'=>$datum->id])}}"><button class="btn btn-success btn-payment-verification">Verify</button></a></td>
            </tr>
            @endif
          @endforeach
          
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
{{-- end table --}}

  {{-- <div class="pad margin no-print">
    <div class="callout callout-info" style="margin-bottom: 0!important;">
      <h4><i class="fa fa-info"></i> Note:</h4>
      This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
    </div>
  </div>

  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">

        </h2>
      </div>
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Dari

        <address>
          <strong>Warisan Gajahmada</strong><br>
          Ruko Crystal 8 no 18 <br>
          Tangerang, Banten<br>
          Phone: 021-50029292<br>
          Email: info@warisangajahmada.com
          <br>
          <br>
          <label for="toko">Cabang/Depo</label>
          <br>
          <p>{{$toko->nama_ukm}}</p>
        </address>

      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        To
        <address>
          <p><strong>{{$pesanan->nama_pemesan}}</strong></p>
          <p>{!! nl2br(e($pesanan->alamat)) !!}</p>
          <p><strong>Phone:</strong> {{$pesanan->phone}}</p>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Detail Transaksi
          <p><b>No Invoice : {{$pesanan->kode}} </b></p>
          <p>Payment : PAID</p>
          <p>Platform : {{$pesanan->platform}}</p>
          <p>Kurir : {{$pesanan->kurir}}</p>
          <h3 class="text-danger">{{$pesanan->status}}</h3>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Product</th>
            <th>Qty</th>
          </tr>
          </thead>
          <tbody>
            @foreach($barang as $row)
          <tr>

            <td>{{$row->productName}}</td>
            <td>{{$row->jumlah}} <sup>{{$row->productNote1}}</sup></td>
          </tr>
            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <!-- <a href="#" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
        <span onclick="window.print();" class="btn btn-default"><i class="fa fa-print"></i> Print</span>

        <a href="{{url('/')}}/{{$pesanan->buktiBayar}}" download class="btn btn-primary" style="margin-left: 5px;">
          <i class="fa fa-download"></i> Invoice
        </a>
      </div>
    </div>
  </section>
  <div id="editor"></div>

  <!-- /.content -->
  <div class="clearfix"></div>

<!-- /.content-wrapper --> --}}

@endsection