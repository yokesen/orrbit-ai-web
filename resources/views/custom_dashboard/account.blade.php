@extends('crudbooster::admin_template')

@section('content')
  <!-- Horizontal Form -->
  
  <div class="box box-info account-form-wrapper">
    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        If you want to change the password, please click your profile picture in the top right corner than click the profile button.
      </div>
      @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
    </div>
    <div class="box-header with-border">
      <i class="fa fa-user-md"></i>
      <h3 class="box-title">Profile</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
  <form class="form-horizontal" action="{{route('updateAccount')}}" method="post" enctype="multipart/form-data">

      <div class="box-body">
        
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">First Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="name"
            value="{{$userData->name}}">
          </div>
        </div>
        <div class="form-group">
          <label for="lastName" class="col-sm-2 control-label">Last Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="lastName" value="{{$userData->lastName}}">
          </div>
        </div>
        <div class="form-group">
          <label for="phoneNumber" class="col-sm-2 control-label">Phone Number</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" name="phoneNumber" value="{{$userData->phoneNumber}}">
          </div>
        </div>
        <div class="form-group">
          <label for="address" class="col-sm-2 control-label">Address</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="address" value="{{$userData->address}}">
          </div>
        </div>
        <div class="form-group">
          <label for="zipCode" class="col-sm-2 control-label">Zipcode</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" name="zipCode" value="{{$userData->zipCode}}">
          </div>
        </div>
        <div class="form-group account-upload-wrapper">
          <label for="photo">File input</label>
          <input name="photo" type="file" id="userPhoto" onchange="document.getElementById('photo').src = window.URL.createObjectURL(this.files[0])">
          <img src="{{url('/')}}/{{$userData->photo}}" alt="" id="photo">
          <p class="help-block">Click the button above to upload the image</p>
        </div>
        
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        @csrf
        <button type="submit" class="btn btn-success account-save">Save</button>
      </div>
      <!-- /.box-footer -->
    </form>
  </div>
@endsection