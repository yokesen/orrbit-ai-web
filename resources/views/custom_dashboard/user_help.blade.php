@extends('crudbooster::admin_template')

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
  <section class="content">
    <div class="container">
        <h1 class="help-title">How can we help you?</h1>
      <div class="row">
        <div class="col">
          <div class="search-field-help">
            <input type="text" placeholder="Search for your issue">
            <button type="submit">
              <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <rect width="15" height="15" fill="url(#pattern0)"/>
                <defs>
                <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                <use xlink:href="#image0" transform="scale(0.00195312)"/>
                </pattern>
                <image id="image0" width="512" height="512" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAMAAADDpiTIAAADAFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACzMPSIAAAA/3RSTlMAAQIDBAUGBwgJCgsMDQ4PEBESExQVFhcYGRobHB0eHyAhIiMkJSYnKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaW1xdXl9gYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXp7fH1+f4CBgoOEhYaHiImKi4yNjo+QkZKTlJWWl5iZmpucnZ6foKGio6SlpqeoqaqrrK2ur7CxsrO0tba3uLm6u7y9vr/AwcLDxMXGx8jJysvMzc7P0NHS09TV1tfY2drb3N3e3+Dh4uPk5ebn6Onq6+zt7u/w8fLz9PX29/j5+vv8/f7rCNk1AAAXi0lEQVQYGe3BCcCPVd438O91/+/FfmOEqxAK7fWkRgptaJNkSZQlZYkIabPcLeqtJkkLlVFa0G5optBCTSZeo0HIFVEm4Q6R7d7/32d93+d5mlGW3znXOdc5nw88z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/M8z/PUy6nV+KyWl3To3qfvgEFDhg4dMqh/n+4dL2t1ZqNa5eAlVYXGF/caOeHVj1ZtLeSBFXz/xUevPn5nj4sal4OXCEHNVn0efH3pDh6q/MUzxvZsXg2etaqeP+TZP2/nkclf8PSA5pXh2aVq29Fvb6Ccta+NOL8SPBsEJw14MaIC6S+e69kwgGewjDOGztxGlb6f0a9xAM9EdW6Y8QN1+PvUbjXgGSV13kOrqFF6cd6ZATwzVLj65e3U77tJbbLgxa3ytW/uY1x2vnRFNrz45HR4o4Dx2vVCmxS8OAStJu+kCTY/djo83eqM+prmWD60Bjx9MjvOKaNZil67KICnxdH3bKKJ1g6vBk+1oOWbpTTV/t+fBk+lnB6f02wLrsqAp0j10VtovnUDK8BToO74vbTDtnuqwxPWZGoJ7bH3sRCeoJNmpGmXwol14Qk55fU07VM8qS48AcdPS9NORU/UhneE6jxXQnvte7AqvCOQ+1AB7bZjeA68w5Q9eBvt9+21AbzDEFy1jsmw+Bx4h+zE95kc046Bd0iqPl7CJNl7Zza8gxZ028qkWXMRvIPU6AMm0fRa8A5Cdl4Rk2nnTQG8X3P2SibXguPh/aIK48qYZPuHp+AdWPO1TLrPGsM7gJyHyph8+wdnwPtnTl9BN3xQB94/CIYW0RU/doL3M7Xeo0umVIT3P7XNp1vWnAbv/0uNTdM1BTcF8P5T7fl00SsV4f27llvoppWN4SEYVEJX/dQeziv3Al12TwbcdvQSum1mJbis6fd03Yr6cFen/fTyz4Gjgrvp/ZvCrnBS5rP0/tOdAdxT8Y/0/p+JKbjmqCX0/tvb5eCWehG9/+njXLjkhO/o/W/LasIdTbfR+7moLlzR/Cd6/+ibhnBDyz30/plNjeGCC/bR++e2nIDku3A/vQPZegKSrtU+ege2pQmS7dy99H7JpoZIsrN/ovfLvq2L5Dp5B71f81UtJFX97+n9uuVVkUy119E7GH8ujySqsozewZmVieTJep/ewXo2QNIEL9I7eHcjae6ndyi6I1l60jskRechSVoU0Ts02xoiORpso3eoVldBUlReReP89O3qZUsWLpj37pz3P1q4dPXGnWU0zZ9SSIbgbRqi6OsFL43td0WzxjUy8XNB7nHN2ve///mPNpTSEA8iGUYyfvv++uLtV9TPwMHIPL7dbS98Xsj4dUYSXJ5mrEqWTuzROAOHKvOUGyZ9XsZY7TkJ9muwk/Epmn/3eeVx+CpdfP/CEsbnq8qwXc5SxmXV+Msq4shVajdxA+PyRgDLPc14LLqtAcQEJ49ayngMht26Mg4Lb60LaQ3vWs4YFJ8FmzXcTe2+G9sQapw6biu1W18F9spaTM2K37w0BXWyrp6TpmbTA1jrQeqVn1cTqjUct4t69YCtLkhTp9U3loMOlQZtoE67G8JOuX+nRgsuCaBLZtcV1GhhClZ6gfosvBBaZXT4G/W5Cza6ktosbhtAt6DTGupSfBrsU2MrNVnTLkAcMvt8T02WZcM6M6jHziFZiEvFvH3UIw+2uZJalE2sgTjVeZVaFJ8Ku+Ruog6fnoq4XbCGOixJwSqTqcHumzMQv5wxRdRgGGzSkhr8qS7M0OQvVG9fPdgjexWV29YtgClSwwqo3GzY404qN682THLCUirXHraov5+KFQ3LgFmyH0xTsW8rwBJ/oGJrzoB5LtpCxR6AHVpTsSkVYKKa86hW0XGwQdZqKlXUF4ZK3Ue1ZsMGt1KpTefAXJfvolKtYb7f7KRKf64FkzVeQ5VWZcJ4j1OlSVkwW+57VKkfTHdcMRUaEcB0mU9RofzKMNwbVKfwGthgSJrq3AuzNaM6O1rADp0Lqcze2jDaR1RmQ2PYotUuKvMUTHYxlVlzDOxxRj5VKW4AcwWLqcqKmrBJ441UZSrM1Z6qLKkOu9RbT0XKmsBUwTIq8mkV2KbOWioyHaZqT0U+qQj7hGupRvoEmClYSjUWV4aN6qynGtNhpsupxrJqsNOxG6lEWSOYKFhEJb48CrZqtJVKPA8TnU8lvg5hr9N3UoXiOjDQe1QhvyFs1qKQKjwO85xGFfafDbt1TFOBvdVgnGlUoKw9bDeIKoyEaeqUUIFBsN8EKrAlB4b5P1RgHBIg9Q4V6AmzVNhBee+lkASVV1He8gBG6Ud5X1dDMhz3I+W1gkmC1RS37xQkRds0xb0Nk1xAedcgOUZTXGkdGOR1ivsdEiTjTxQ3FuYISyjts0wkyW++o7QtWTDGaErb3QDJ0qKM0jrCFKmNlHYdkmYMpc2DKdpQ2jQkTuYiCksfC0O8SmEbcpE8x+2lsHthhuqFFHY+kmgAhX2TASMMorBnkUgZ8ynsQhjh/1LW97lIpob7KOsFmKARhbVHUo2grN3lYYA8ynodiZW5nLKuQfyCryhqd20kVzPKmon4NaWs25Fkz1NUYS5i9zBFrctBktXcRVE9ELdgHUW1R7INp6h3ELfTKOqDAMmWs56SiqogZvdTUtkpSLrOFHUtYraKkl5B4gVLKel1xKsBJZU2QvK1paTd2YjVYEp6AQ4IPqGktojV+xRU0gAuuIiSJiBOlYsp6Dk4IVhIQesQp6spqKwB3HAZJTVCjCZR0JtwRPAFBQ1CjNZRUHO44noKegfxqU9Bi+CM7M2UszsTselLQV3gjtEU1ByxmUE5GzPhjppFlDMacQk2UU4eXDKDcj5AXBpSTroeXHIB5ezLQkxuoJy5cEqwlnKaISZTKecauGUU5YxATL6imO05cEu9NMXMRDyqUc5TcM18itkSIBaXUE4ruKYf5dRHLPIoZmsKrqlRSjFdEIt3KWYS3DOPYsYhFlsp5iK452aKmY841KaYbZlwzzEUsytADC6jmBfhor9STEPEYCTFdIOL7qeYqxGDNyjmKLioOcXcixisppS/wUmpXZTyJvTLKqGUh+GmWZSyBvqdQDEXw01DKKU0G9p1pJTi8nDT6RRzIrQbTSlL4KjUT5RyNbR7kVKegqvmUsod0O5TSrkOrrqHUiZDuy2UcjxcdTmlzIduFSllewBX1aSUb6DbKZQyD+76jkLKsqDZ5ZQyHu56l1KOhWb9KOVGuOthSmkFzcZSSnO46zpKuQ6aTaWUXLjrLEq5A5p9QCGb4LAqlPIkNPuCQj6Ey7ZQyOvQbCuFPA+X/YVCPoFeqTIKuQcum04hX0KvoyjlBrjsAQr5AXqdTCmt4bKbKaQsA1qdRymN4bL2lFIVWl1BKRXgsqaU0hBadaeQvXBaXUo5E1oNpJDv4LTylHIBtLqLQlbAbXsppD20GkshC+C27yikG7QaRyEz4bZVFHITtJpEIVPgts8oZAi0mkohE+C2eRRyO7R6jUIegdtmUUgetHqbQsbCba9SyFhoNYtC8uC2qRTyMLR6h0LugtumUMg4aPUuhQyH256jkMeg1VwKGQK3TaKQCdBqLoUMgdueoZDx0GoOhQyD2yZTyDho9ScKGQG3PU8hj0CrWRRyN9w2jUIeglZvUsj9cNtbFHIftJpGIb+D296lkJHQagqFTITbPqaQ26DV0xTyCty2jEJugVaPUMg7cNt6CukNrfIo5BO4bQeFdIFWwyhkNZyWSlPIZdDqRgr5AU47ilJaQqsuFFKWgstOopRToVVrSqkNl11MKXWgVVNKORMu60kplaDVsZRyJVx2N4WUBNCqIqXcApc9SyGboVewn0LGwWVzKWQFNNtAIW/DZV9RyEfQ7DMKWQGHZRZTyAxo9haFFGTAXY0oZRw0e5JS6sNd7ShlODS7g1Iuh7tup5Ru0OxaSrkT7nqFUlpAs3MpZTrctZxS6kGzkFK+hLNyiimkNBOaBQUUkq4EV51NKRuh3ZeU0hKuGkgpC6DdbEoZAVe9RClToN2jlPIHuOprSrkL2t1EKfkB3FSLYrpAuxYU0wRu6kwxp0C7GhQzAG6aRCllOdBvK6W8ATdFlLIWMfiQUran4KK6FDMLMRhPMc3gor4UMxYx6E0x98JFMynmGsTgDIpZCgfl7KaYExGD7CKKqQv3XEYx+1KIw18pZhDc8xzF/AWxmEQx8+GczHyKeQqxuIFi0iFcczHl9EYsTqKcwXDNZMo5EbHI2Ekxi+GYnB8pZlcG4jGHcprALZ0pZx5iMopyHoJb/kg59yIm51PO5iy45JhSyrkYMSlfRDkd4JLRlFNaCXH5hHLmwiGpbylnCWJzHwU1gjuuoqBHEJtWFPQE3DGfgi5FbLL3Uc7uXLjiNAoqqYT4vEtBI+CKlyloIWI0hIK+z4Yb6pVQ0BjEqBEl3Qg3TKCksxGndRS0PgsuCAsoaHsKcZpASb3ggscoaRpidRElrc9C8oX7KakrYpW1i5IGIPkmUlJJVcRrGiVtroCka1hCSfMRs04UNQpJ9zpF3YKYVdxPSXtqI9maU9YxiNtbFDUZiZaxiKIWIXZdKSrdFEnWi7JuQ+wq7qOoRRlIrtx8yqqL+M2grJuQXE9S1qcwQDvK+rEmkuq3acoaBANkbaOs6UiorOWUVVIDJniKwtohmUZS2GwYoSmFfV8NSXRyEYV1ghGClRT2EhIoawmF7ciBGYZRWickz72U9gQMUaOIwnYcjaQ5p5TSToMpXqW0D1NIltwNlLYExriA4kYiUYLXKe5GGCNYQ2mlLZEk/ShuV0WYYzDFba6N5DiriOImwCBV9lDcx5lIiurfUl4TmORpyhuPhMicR3lzYJQTqEAvJMOjVOBSmGUO5RU1RxL0oAJRBszShgr80AD2a1lEBfrCMMEXVGB1Vdju+B1UIL8cTNOTKszPgd1qfk0VRsM42ZuowmsZsFmlv1KFPdVhnmFUYkIAe2W/RyV+BwNV2kEl8mCt1GtUorA2TJRHNYbAUsFzVGMijFRtN9XoBysFE6hGYR2Y6QEq0gcWCh6hIk/CUL/ZTTXSN8A6wUNUpOBomOoBqtIPlgnGU5VHYaxqu6jKEFglYyJV2V0D5hpDZcYEsEfWK1QmDwar9AOVGZ8BW5SfTWW2VobJBlOd6dmwQ/W/UJ3+MFrOBqrzYRXY4Ng1VOfLTJjtWiq08liY7+ytVOgKGC5jCRXa+luYruN+KjQ3gOlaUKWC62C0jDyqVHoSzPcGlfpdCuaq9BaVehIWqF9IpT44CqY6YTWV+qEqbDCWan13DszUeQ/V6gUrVPg71Sq5LQPmKfc0Ffs0gB06U7X3asI0JyynYiWnwhLBXKqWfwWMEty8n6o9BGs0KqRyz1aGOY5+l8qtrwB7jKF637aGIYLeO6leG1gkZw01mFIdJmgwhxq8AKucRx3yuweIW9bt+6nBlmqwy0RqMf9kxOvCVdTiKlim8rfUovTxaojPsa9Rj5dhndbUZMet2YhH7sOF1GNTNdjnGeqy/voU9Cs/fBt16Q4LVdpAbVZ2yoBeOQM2UZ8ohIVapKnPymtS0Kf8oO+oVRTCQg9Rp3X9y0GPaqPyqVsUwj7Zn1Or/PtCqNfk6b2MQRTCPk32Ua/iGecHUCmrwxzGJAphnxuoXXR7CFWaPLiZ8YlCWCeYTv3K5lxfGfJqDVzEeEUhrFN5LeNQ8Ha3XEiq3f/DMsYuCmGd0wsYj+J5gxtBRPAvd3+WphGiENbpw/h8/UyXGjgy9Xq/vIXmiEJY5/eM1erJvZpk4HBknTFg2jc0TBTCNuWWMG4/zR/fu2kFHLzccwdMXFRAE0UhbFM3n0bY+P7EEZ3OClM4sKx6zbuN/P3HW2mwKIRtWhTTIKWbln3w6jMP3z2kb49rOnW4skOna3r2Hzr60clvzF+5NU0LRCFs05eeoCiEbZ6gJygKYZnM9+gJikJYpsoX9ARFISxT53t6gqIQljl9Nz1BUQjLXFRMT1AUwjJd0/QERSEsM4iepCiEZfLoSYpC2CUYR09SFMIuwSR6kqIQdsl4gZ6kKIRdUi/RkxSFsEvqJXqSohB2Sb1AT1IUwi4Zz9CTFIWwSzCenqQohF2Ce+hJikJYZgg9SVEIy3QvoScoCmGZtnvoCYpCWObMLfQERSEsU28VPUFRCMvkvk9PUBTCMlmT6AmKQthmYCk9OVEI21y4nZ6cKIRt6i+jJycKYZsKL9GTE4WwTTCgiJ6YKIR1zv6GnpgohHWqzqQnJgphnWBgIT0pUQj7nLqKnpQohH3KP0FPShTCQm020RMShbBQtZeZeLtmUIsohI2u2sJkm1cH91KLKISNqr3IBNszIABwL7WIQlipzTdMqjn18B/uoRZRCCtVfKSUSfRjrwD/JY9aRCHsdOpCJs/LR+G/jaEWUQg7ZfT+gcmyrjX+lzHUIgphqaoTSpkcBaPL4WdGU4sohK1OmsukmFUf/2gUtYhC2Cq4bDWTYFUb/FMjqUUUwlqZ/bbQdtsHZeIARlKLKIS9Ko75iTYrfDgXB3Y3tYhCWOw3jxbQVumXjsUvuotaRCFsFj5VRCvNPBm/5k5qEYWwWp2ni2idOb/FQbiTWkQh7Hb0+P20yvvNcXDuoBZRCMvVuH8nrTGrGQ7a7dQiCmG7ykM30gal007BobidWkQhrJd17RKabs9j9XCIRlCLKIT9gnNfL6XBvhleFYfuNmoRhUiCOvdvpaE+6pDCYRlOLaIQiZDd5UOaZ+fjTXDYhlOLKERCNHxgE42y4PryOBLDqEUUIilSbWfspyG+GXscjtQwahGFSI7KPeeVMnY7p7TKgICh1CIKkSQ1By5IM0Z7prXLhpBbqUUUIllq959Xwlj8OLVdOQi6lVpEIZImt+u07dRszaPnZ0LYEGoRhUie1Ln3LS6jJntnDzwOKgymFlGIRKrecdKXVK3o47wW2VDlFmoRhUiqWl2e+FspFflp7qhW5aHULdQiCpFglS64a+Ymyir+fHKfkzKg3iBqEYVIuJqX3jVjdQkFbF/w5E1n5kCXgdQiCuGA7FM6j562eAcPT/HaOU/efEHtAHoNpBZRCGdU/ZeOQx9/47ONhTwYZZs//+Pk0T3Or59CPG6mFlEI1wS5x5/TrseteeMmT/vD3I8XL12x6sto9coVf1v08bzZ058bf9/Q3le3PLFGBuI2gFpEITwzDaAWUQjPTP2pRRTCM1M/ahGF8MzUj1pEITwz9aUWUQjPTH2pRRTCM9NN1CIK4ZnpxjR1iEJ4ZuqTpg5RCM9MfdLUIQrhmemGNHWIQnhm6p2mDl/WgGem3mnqsKQiPDP1SlOHtzPgmalXmjqMhGeonmlqUNYCnqF6pKnBhorwDNUjTQ0eg2eq68uoXulJ8Ex1fRnVmw3PWNeVUb2m8IzVvYzKvQHPXN3LqFppHXjm6lZG1UbBM1i3Miq2Gp7Jri2jYo3hmaxrKdW6FZ7RupZSqVnwzHZNKVXKD+CZrUspVToGnuG6lFKh1vBM17mU6vSHZ7zOpVRmLDzzdSqhKs/As0DHEiryKjwbdCyhGrPhWeHqEirxR3h2uLqEKvwBniU6lFCBafBs0aGE8p6GZ42riiluNDx7tC+mtB7wLNK+mMKawbPJlcUUla4EzypXFlPSKniWaVdEQZPg2aZdEeVcDc86VxRRSkElePa5vIhC3oJno8uLKONKeFa6rIgSNmbCs9NlhRQwDJ6tLi3kEdtaAZ61LinkkRoIz2KXFPLIrMyEZ7NLCnkk0ufCs1vbAh6B8fBs17aAh215OXjWa1PAw7TreHgJ0LqAh6X0EniJ0LqAh+MmeAlxcQEP3R3wEqN1IQ/V3fASpPU+HpL0QHiJ0mwbD8G+DvASpsEXPGhfnw4vcSpM4UGakQsvia7azIOwuSO8hKryWDF/RdGjVeAlV/3JxfwFRc/Ug5dstcds4AGsH1ULXvIFzR5elubPpJc9/NsAniuqthkx5cM1+QVpFm1f+/HUOy+thkPyr39wLQ2BNHOIAAAAAElFTkSuQmCC"/>
                </defs>
              </svg>
            </button>
          </div>
        </div>
      </div>
      <div>
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="box-group" id="accordion">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-dark">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    User Help
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="box-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                  wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                  eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                  nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                  farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                  labore sustainable VHS.

                </div>
              </div>
            </div>
            <div class="panel box box-danger">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    User Help
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse">
                <div class="box-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                  wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                  eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                  nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                  farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                  labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    User Help
                  </a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse">
                <div class="box-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                  wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                  eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                  nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                  farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                  labore sustainable VHS.
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <h1 class="h1-help-contact">If you can't find the answer, please don't be hesitate to contact us</h1>
      <div class="col btn-help-wrapper">
        <button class="btn btn-success btn-help">
          Contact Us
        </button>
      </div>
    </div><!-- /.container-fluid -->
  </section>
</div>
@endsection
