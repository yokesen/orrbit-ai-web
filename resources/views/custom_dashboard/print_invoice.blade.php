
@extends('crudbooster::admin_template')

@section('content')
  <!-- Your Page Content Here -->
  <div class="pad margin no-print">
    <div class="callout callout-info" style="margin-bottom: 0!important;">
      <h4><i class="fa fa-info"></i> Note:</h4>
      This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
    </div>
  </div>

  <!-- Main content -->
  
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        {{-- <h2 class="page-header">
          <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">

        </h2> --}}
      </div>
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Dari
        
        <address>
          <strong>Orrbitt AI</strong><br>
          Address <br>
          City<br>
          Phone: 021-50029292<br>
          Email: info@Orrbit.com
          <br>
          <br>
          {{-- <label for="toko">Cabang/Depo</label> --}}
          {{-- <br> --}}
          <p></p>
        </address>

      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        To
        <address>
          <p><strong></strong></p>
          <p></p>
          <p><strong>Phone:</strong> </p>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Detail Transaksi
          <p><b>No Invoice : HJPEBL </b></p>
          <p>Payment : PAID</p>
          <p>Platform : </p>
          <p>Kurir : </p>
          <h3 class="text-danger">keranjang</h3>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Product</th>
            <th>Qty</th>
          </tr>
          </thead>
          <tbody>
                      <tr>

            <td>MP2</td>
            <td>1 <sup></sup></td>
          </tr>
            
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <span onclick="window.print();" class="btn btn-default"><i class="fa fa-print"></i> Print</span>

        

        
      </div>
    </div>
  </section>
  <div id="editor"></div>

  <!-- /.content -->
  <div class="clearfix"></div>

  <!-- /.content-wrapper -->
@endsection