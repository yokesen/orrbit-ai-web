@extends('crudbooster::admin_template')


@section('content')

  <div class="report-form-wrapper">
    <h1 class="report-title">Report</h1>
    <div class="box box-info">
      <div class="pad margin no-print">
        @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form-horizontal" action="{{route('submitReport',['id'=>$id])}}" method="post">
          <div class="box-body">
            @csrf
            <div class="form-group">
              <label for="title">TITLE</label>
              <input type="text" class="form-control" id="title" name="title" value="">
            </div>
            <div class="form-group">
              <label for="product">COMPLAINED PRODUCT</label>
              <input type="text" class="form-control" id="product" name="product" value="{{$getData[0]->productName}}">
            </div>
            <div class="form-group">
              <label>CATEGORY</label>
              <select name="selectCategory" class="form-control">
                <option value="" disabled selected>Please choose the categories</option>      
                @foreach ($ticketCategories as $category)
                  <option value="{{$category->nmCategoryTickets}}">{{$category->nmCategoryTickets}}</option>      
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>COMPLAIN</label>
              <textarea class="form-control" rows="9" placeholder="Enter your complain...." name="complain"></textarea>
            </div>
          </div>
          <!-- textarea --> 
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
    <!-- /.box -->
  </div>
  
@endsection