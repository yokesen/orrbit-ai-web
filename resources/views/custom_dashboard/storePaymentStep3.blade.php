@extends("crudbooster::admin_template")
@section("content")
  

  @foreach($robots as $robot)
  <div class="row">
    <div class="col-md-12">
      <h3>Pemesanan Robot</h3>
      <p>Step 2 (of 2) : Confirm Payment</p>
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <div class="image-150">
            <img class="storePayment1-img" src="{{url('/')}}/{{$robot->pictures}}" alt="" width="100%"/>
          </div>
        </div>
        <div class="box-body">
          <h4>Robot {{$robot->productName}}</h4>
          <br>
          <p>Dapat keuntungan dengan sistem Swap Arbitrage bersama Orrbit AI</p>
        </div>
      </div>
    </div>  
  
  @endforeach
  
    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h4>Pembayaran</h4>
        </div>
        <div class="box-body">
          Silahkan lakukan pembayaran sebagai berikut :
          <table class="table">
            <tr>
              <td>Nominal</td>
              <td>: <b>Rp {{number_format($anggaran,'0','.','.')}},00</b></td>
            </tr>
            <tr>
              <td colspan="2"><b>Kepada</b></td>
            </tr>
            <tr>
              <td>Bank</td>
              <td>: <b>Bank Central Asia</b></td>
            </tr>
            <tr>
              <td>No Rekening</td>
              <td>: <b>6840379530</b></td>
            </tr>
            <tr>
              <td>Nama Rekening </td>
              <td>: <b>Eko Prihartono</b></td>
            </tr>
          </table>
          <div class="form-group header-group-0 row">
            @csrf
            <input type="submit" name="submit" value="Confirm Payment" class="btn btn-success btn-confirm-payment">
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection