@extends('crudbooster::admin_template')

@section('content')
<section class="pricing py-5">
  <h1 class="store-title text-center">Store Center</h1>
  <div>
    <select id="selectProduct" class="select-store form-control">
      <option class="select-placeholder" selected disabled>Please choose the product...</option>
      @foreach ($getProducts as $product)
        <option value="{{$product->id}}">{{$product->productName}}</option>
      @endforeach
    </select>
  </div>
  <div class="container">
    
    <div class="row">
      {{-- Plan 1 --}}
      @foreach ($getProducts as $product)
        @if ($product->periodMonth == 1 || $product->periodMonth == 6 )
          <div class="col-lg-4 plan1">
            <div class="card mb-5 mb-lg-0">
              <div class="card-body">
                @if ($product->periodMonth == 1) 
                  <h3 class="card-title text-center">{{$product->periodMonth}} month</h3>                    
                @else
                  <h3 class="card-title text-center">{{$product->periodMonth}} months</h3>                    
                @endif
                <h2 class="card-price text-center">$ {{$product->periodPrice}}</h2>
                <a href="{{route('storePaymentStep1', ['id'=>$product->id])}}" class="btn btn-block text-uppercase">buy now</a>
                <hr />
                <ul class="fa-ul">
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Orrbit Expert Advisors
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Orrbit Full User Guide with many Best Practise Tips and Trick
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Orrbit Portfolio Research and Record
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Free Build Updates
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Personalized 24/7 Support
                  </li>
                </ul>
              </div>
            </div>
          </div>  
        @elseif ($product->periodMonth == 12)
          <div class="col-lg-4 plan2">
            <div class="card mb-5 mb-lg-0">
              <div class="card-header">BEST DEAL!</div>
              <div class="card-body">
                <div class="card-title-plan2">
                  <h3 class="card-title text-center">{{$product->periodMonth}} months</h3>
                </div>
                <h2 class="card-price text-center card-price-plan3">$ {{$product->periodPrice}}</h2>
                <a href="{{route('storePaymentStep1', ['id'=>$product->id])}}" class="btn btn-block text-uppercase text-buy-plan2">buy now</a>
                <hr />
                <ul class="fa-ul">
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Orrbit Expert Advisors
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Orrbit Full User Guide with many Best Practise Tips and Trick
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Orrbit Portfolio Research and Record
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Free Build Updates
                  </li>
                  <li>
                    <span class="fa-li"><i class="fas fa-star-of-life"></i></span
                    >Personalized 24/7 Support
                  </li>
                </ul>
              </div>
            </div>
          </div>
        @endif
      @endforeach
      
    </div>
  </div>
</section>
@endsection
