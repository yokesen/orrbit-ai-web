<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => \UriLocalizer::localeFromRequest(), 'middleware' => 'localize'], function(){
    Route::get('/lab', 'dashboardMenu@userHelpMenu');
    Route::get('/', 'PageController@pageIndex')->name('homePage');
    Route::get('/pricing', 'PageController@pagePricing')->name('pricingPage');
    Route::get('/swap-arbitrage', 'PageController@pageSwap')->name('swapPage');
    Route::get('/technology', 'PageController@pageTechnology')->name('techPage');
    Route::get('/blog', 'PageController@pageBlog')->name('blogPage');
    Route::get('/blog/1', 'PageController@pageArticle')->name('articlePage');
    Route::get('/register', 'PageController@pageRegister')->name('registerPage');
    Route::get('/cart', 'PageController@pageCart')->name('cartPage');
    Route::get('/history', 'PageController@pageHistory')->name('historyPage');
    Route::get('/coupon', 'PageController@pageCoupon')->name('couponPage');
    Route::get('/collection', 'PageController@pageCollection')->name('collectionPage');
    // Route Process Cart
    Route::get('/remove/cart/{id}', 'processController@removeFromCart')->name('removeFromCart');
    Route::get('/rentPeriode/{service}/{cartid}', 'processController@rentPeriode')->name('rentPeriode');
    Route::get('/addService/{service}/{cartid}', 'processController@addService')->name('addService');
    Route::get('/removeService/{service}/{cartid}', 'processController@removeService')->name('removeService');
    Route::get('/kodeVoucher/{kodevoucher}', 'processController@kodeVoucher')->name('kodeVoucher');
    Route::get('/removeVoucher', 'processController@removeVoucher')->name('removeVoucher');

    Route::middleware('UserLogin')->group(function(){
        Route::get('/privacy-policy','UserController@privacyPolicy')->name('privacyPolicy');
        Route::get('setting/help','UserController@help')->name('help');
        Route::get('setting/helpManageAccount','UserController@helpManageAccount')->name('helpManageAccount');
        Route::get('setting/help/paymentMethod','UserController@paymentMethod')->name('paymentMethod');
        Route::get('setting/help/installRobot','UserController@installRobot')->name('installRobot');
        Route::get('setting/help/manageVPS','UserController@manageVPS')->name('manageVPS');
        Route::get('/setting/complain','UserController@complain')->name('complain');
        Route::get('/setting/profile','UserController@settingProfile')->name('profile');
        Route::post('/setting/submitProfile','UserController@submitProfile')->name('submitProfile');
        Route::get('/setting/changePassword','UserController@changePassword')->name('changePassword');
        Route::post('/submit/changePassword','UserController@submitChangePassword')->name('submitChangePassword');  
        Route::get('/setting/test','UserController@test')->name('test');
        Route::post('/setting/submitBlog','UserController@submitBlog')->name('submitBlog');
    });
    Route::middleware('SuperAdminLogin'||'WebAdminLogin')->group(function(){
       
    });
    Route::middleware('SuperAdminLogin'||'WebAdminLogin' || 'UserLogin' )->group(function(){
        Route::get('/setting/complain/content/view','UserController@complainContentView')->name('complainContentView');
        Route::post('/setting/complain/content/{ticket}','UserController@complainContent')->name('complainContent');
        Route::post('/post-chat/','UserController@postChat')->name('postChat');
    });
    Route::group([
        'prefix' => '/admin',
        'middleware' => [
            'SuperAdminLogin'||'WebAdminLogin'
            ]
        ], function () {
            Route::get('adminHome','dashboardMenu@adminHome')->name('adminHome');
            Route::get('/report/detail/{id}','UserController@reportDetail')->name('reportDetail');
            Route::get('/report/button/{id}','dashboardMenu@reportButton')->name('reportButton');
            Route::get('/payment-verification/{id}','UserController@paymentVerification')->name('paymentVerification');
            Route::get('/print-receipt/{id}','UserController@printReceipt')->name('printReceipt');
    });
    // Route::group([
    //     'prefix' => '/admin',
    //     'middleware' => [
    //         'SuperAdminLogin'||'WebAdminLogin'||'UserLogin'
    //         ]
    //     ], function () {
    //     });
        
    });
    

// home
// Route::group([
//     'prefix' => '/admin',
//     'middleware' => [
//         'SuperAdminLogin'||'WebAdminLogin'||'CrmLogin'||'CmsLogin'
//         ]
//     ], function () {
//         Route::get('adminHome','dashboardMenu@adminHome')->name('adminHome');
// });

// verify and payment
Route::group([
    'prefix' => '/admin',
    'middleware' => [
        'SuperAdminLogin'||'WebAdminLogin'
        ]
    ], function () {
        // Route::get('/payment/verification/{id}',[
        //     'uses'=> 'dashboardMenu@paymentVerification',
        //     'as'=>'paymentVerification'
        // ]);

        Route::get('/payment/confirmation','dashboardMenu@paymentConfirmation')->name('paymentConfirmation');
        Route::get('/tes/button/{id}','dashboardMenu@tesButton')->name('tesButton');
});

/*-----------------------PROCESS CONTROLLERR---------------------------*/
Route::post('/registering', 'UserController@postRegister')->name('processRegistering');
Route::get('/userLogout','UserController@userLogout')->name('userLogout');

/*-------------------------------------------------------*/
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// masukin ke dalam url buat user pad baris 16
Route::middleware('UserLogin')->group(function(){
    Route::prefix('/admin')->group(function(){
        Route::get('/user/printReceipt/{id}',[
            'uses'=>'dashboardMenu@printReceipt',
            'as'=>'printReceipt'
            ]);
        Route::get('/user/storePaymentStep1/{id}',[
            'uses'=>'dashboardMenu@storePaymentStep1',
            'as'=>'storePaymentStep1'
            ]);
        Route::get('/user/storePaymentStep2/{id}',[
            'uses'=>'dashboardMenu@storePaymentStep2',
            'as'=>'storePaymentStep2'
            ]);
        Route::get('user-help','dashboardMenu@userHelp')->name('userHelp');
        Route::get('store','dashboardMenu@store')->name('store');
        Route::get('account','dashboardMenu@account')->name('account');
        Route::get('home','UserController@userHome')->name('userHome');
        Route::post('/update/account','UserController@updateAccount')->name('updateAccount');

        Route::get('/user-report','dashboardMenu@userReport')->name('userReport');
        Route::get('/user/report/{id}','dashboardMenu@userReportId')->name('userReportId');
        Route::post('/user/submitReport/{id}','UserController@submitReport')->name('submitReport');

    });
});


Route::prefix('/staging')->group(function(){
    Route::get('guzzle','LabYokeController@guzzel')->name('labGuzzle');
    Route::get('price','LabYokeController@price')->name('labPrice');
});

//Route by Andika
Route::get('add-to-cart/{id}/{month}','processController@addToCart')->name('addToCart');
Route::get('/use-voucher/{kode}','processController@useVoucher')->name('useVoucher');
Route::get('/buy-again/{id}','processController@buyAgain')->name('buyAgain');
Route::get('/processCheckout','processController@processCheckout')->name('processCheckout');

Route::get('/getModalData', 'processController@getModalData')->name('getModalData');
//Route Lab
Route::get('/lab/shopingcart','processController@shopingCart')->name('shopingCart');

