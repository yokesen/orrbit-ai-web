<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Robots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('robots', function (Blueprint $table) {
            $table->increments('id');
            $table->text('robotFile');
            $table->string('robotLongName',100);
            $table->string('robotShortName',50);
            $table->string('robotSlugName',100);
            $table->text('robotImage');
            $table->text('robotDescription');
            $table->float('robotPrice1');
            $table->float('robotPrice2');
            $table->float('robotPrice3');
            $table->string('robotStatus', 50);
            $table->string('robotType', 255);
            $table->text('robotAttribute1');
            $table->text('robotAttribute2');
            $table->text('robotAttribute3');
            $table->text('robotAttribute4');
            $table->text('robotAttribute5');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robots');
    }
}
