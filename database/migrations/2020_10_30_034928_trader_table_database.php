<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TraderTableDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('traders', function (Blueprint $table) {
          $table->increments('id');
          $table->string('userId')->nullable();
          $table->string('licenseKey')->nullable();
          $table->string('broker')->nullable();
          $table->string('server')->nullable();
          $table->string('account')->nullable();
          $table->string('pair')->nullable();
          $table->string('status')->nullable();
          $table->string('master')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traders');
    }
}
