<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCmsUserColumnNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->string('lastName')->nulllable()->change();
            $table->string('phoneNumber', 50)->nulllable()->change();
            $table->string('address')->nulllable()->change();
            $table->string('zipCode', 50)->nulllable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->dropColumn('lastName')->delete();
            $table->dropColumn('phoneNumber')->delete();
            $table->dropColumn('address')->delete();
            $table->dropColumn('zipCode')->delete();
        });
    }
}
