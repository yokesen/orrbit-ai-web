<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RobotDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('robot_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('robotId');
            $table->foreign('robotId')->references('id')->on('robots');
            $table->string('details1');
            $table->string('details2');
            $table->string('details3');
            $table->string('details4');
            $table->string('details5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robot_details');
    }
}
