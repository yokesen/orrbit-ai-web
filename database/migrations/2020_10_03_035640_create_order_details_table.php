<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('orderDetailsId');
            $table->unsignedInteger('customerId');
            $table->unsignedInteger('productId');
            $table->unsignedInteger('idProductPeriods');
            $table->foreign('customerId')->references('id')->on('cms_users');
            $table->foreign('productId')->references('id')->on('products');
            $table->foreign('idProductPeriods')->references('idProductPeriods')->on('product_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
