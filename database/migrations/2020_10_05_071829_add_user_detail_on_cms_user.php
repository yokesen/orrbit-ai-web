<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserDetailOnCmsUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->string('lastName')->nulllable();
            $table->string('phoneNumber', 50)->nulllable();
            $table->string('address')->nulllable();
            $table->string('zipCode', 50)->nulllable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->dropColumn('lastName')->nulllable();
            $table->dropColumn('phoneNumber')->nulllable();
            $table->dropColumn('address')->nulllable();
            $table->dropColumn('zipCode')->nulllable();
        });
    }
}
