<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketConversation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketConversations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ticketId');
            $table->foreign('ticketId')->references('id')->on('tickets');
            $table->unsignedInteger('userId');
            $table->foreign('userId')->references('id')->on('cms_users');
            $table->longText('chat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketConversations');
    }
}
