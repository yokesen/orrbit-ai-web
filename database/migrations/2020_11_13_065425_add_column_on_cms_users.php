<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnCmsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->string('providerId')->nulllable();
            $table->string('providerOrigin')->nulllable();
            $table->string('parent')->nulllable();
            $table->string('utm_source')->nulllable();
            $table->string('utm_medium')->nulllable();
            $table->string('utm_campaign')->nulllable();
            $table->string('utm_term')->nulllable();
            $table->string('utm_content')->nulllable();
            $table->string('uuid')->nulllable();
            $table->string('ipaddress')->nulllable();
            $table->string('screen')->nulllable();
            $table->string('platform')->nulllable();
            $table->string('platformVersion')->nulllable();
            $table->string('device')->nulllable();
            $table->string('browser')->nulllable();
            $table->string('browserVersion')->nulllable();
            $table->string('language')->nulllable();
            $table->string('robot')->nulllable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->dropColumn('providerId')->nulllable();
            $table->dropColumn('providerOrigin')->nulllable();
            $table->dropColumn('parent')->nulllable();
            $table->dropColumn('utm_source')->nulllable();
            $table->dropColumn('utm_medium')->nulllable();
            $table->dropColumn('utm_campaign')->nulllable();
            $table->dropColumn('utm_term')->nulllable();
            $table->dropColumn('utm_content')->nulllable();
            $table->dropColumn('uuid')->nulllable();
            $table->dropColumn('ipaddress')->nulllable();
            $table->dropColumn('screen')->nulllable();
            $table->dropColumn('platform')->nulllable();
            $table->dropColumn('platformVersion')->nulllable();
            $table->dropColumn('device')->nulllable();
            $table->dropColumn('browser')->nulllable();
            $table->dropColumn('browserVersion')->nulllable();
            $table->dropColumn('language')->nulllable();
            $table->dropColumn('robot')->nulllable();
        });
    }
}
