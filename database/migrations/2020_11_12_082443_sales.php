<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice');
            $table->string('salesCode');
            $table->integer('user_id');
            $table->longText('salesSart');
            $table->float('salesSubtotal');
            $table->float('salesDiscount');
            $table->string('salesVoucher',50);
            $table->float('salesTotal');
            $table->string('salesStatus',50);
            $table->dateTime('salesTime');
            $table->string('salesPaymentStatus', 50);
            $table->dateTime('salesPaymentTime');
            $table->string('salesPaymentBukti', 50);
            $table->string('salesDownloadStatus', 50);
            $table->dateTime('salesDownloadTime');
            $table->string('salesInstallStatus', 50);
            $table->dateTime('salesInstallTime');
            $table->string('salesMaintainStatus', 50);
            $table->dateTime('salesMaintainTime');
            $table->string('salesVpsStatus', 50);
            $table->dateTime('salesVpsTime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robot_details');
    }
}
