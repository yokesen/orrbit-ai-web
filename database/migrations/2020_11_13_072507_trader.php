<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Trader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trader', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('userId')->nullable();
            $table->string('licenseKey')->nullable();
            $table->text('broker')->nullable();
            $table->text('server')->nullable();
            $table->text('account')->nullable();
            $table->string('pair')->nullable();
            $table->string('status')->nullable();
            $table->string('master', 10);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trader');
    }
}
