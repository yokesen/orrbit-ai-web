<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TradeProfit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_profit', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->text('server');
            $table->integer('login');
            $table->integer('bln');
            $table->integer('thn');
            $table->float('profit');
            $table->float('fee');
            $table->string('lunas');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_profit', function (Blueprint $table) {
            //
        });
    }
}
