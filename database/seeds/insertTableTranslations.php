<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class insertTableTranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('translator_languages')->insert([
            'locale' =>'in',
            'name' => 'india',
            'created_at' => Carbon::now(),
        ]);
    }
}
